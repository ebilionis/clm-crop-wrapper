"""

.. _output:

===================
Reading the Outputs
===================

CLM-Crop has lots of outputs. Typically, when calibrating the model we are only
interested in looking at the few of them that are measurable. We can do this
by exploiting the class :class:`clm.Output` which works very similarly to
:class:`clm.Input`. To initialize a :class:`clm.Output` object you need a valid
:class:`clm.Case` object and a list of the variables you want to look at::

    >>> output_list = [{'name': 'LEAFC', 'idx': 20},
    ...                {'name': 'TLAI', 'idx': 20},
    ...                {'name': 'ORGANC', 'idx': 20},
    ...                {'name': 'STEMC', 'idx': 20},
    ...                {'name': 'GPP', 'idx': 20},
    ...                {'name': 'NEE', 'idx': 4},
    ...                {'name': 'GDDFRAC', idx: 20}]
    >>> output = clm.Output(case, output=output_list)

As you can see the format of the ``output_list`` is exactly the same as the format
we used for the ``free_params`` in :class:`clm.Input`.
Specifying the index as 'all' means that you want to look at all the dimensions of
that specific variable. You may get the total number of outputs by using
:attr:`clm.Output.num_output`.

Now, :class:`clm.Output` exposes the variables you specify as attributes. However,
these attributes **should not be accessed before the case has actually been run**.
This will result in an exception being thrown, if the CLM history files are missing.
Assuming that you have actually run the case, all you have to do to access a variable
is use its name:

    >>> print output.LEAFC
    >>> print output.TLAI

In general, if the name of the variable is stored in the string ``name``, then do::

    >>> print getattr(output, name)

If you want to get all the outputs as a 1D array, you may use the method
:meth:`clm.Output.get_as_array()`.

Of course, you are not allowed to set the output variables. An exception will be raised
if you try to do so.

"""


__docformat__ = 'reStructuredText'


__all__ = ['Output']


import numpy as np
import os
from netCDF4 import Dataset
from . import Case
import pandas as pd


class Output(object):

    """
    Represents the output of CLM-crop.
    """

    # The case
    _case = None

    # The output you want to look at
    _output = None

    # The total number of outputs
    _num_output = None

    @property
    def case(self):
        """
        :getter:    Get the case.
        :type:      :class:`clm.Case`
        """
        return self._case

    @property
    def output_file(self):
        """
        :getter:    Get the output file.
        :type:      str
        """
        return self.case.output_file

    @property
    def output(self):
        """
        :getter:    Get the output you want to look at.
        :type:      list of dict
        """
        return self._output

    @property
    def num_output(self):
        """
        :getter:    Get the total number of outputs.
        :type:      int
        """
        return self._num_output

    def _get_dict_from_str(self, out):
        """
        Get a dictionary representation of the output.
        """
        out_dict = {}
        out_dict['name'] = out
        out_dict['idx'] = 'all'

    def _check_dict(self, out):
        """
        Check if the dictionary is of the right form.
        """
        assert out.has_key('name')
        if not out.has_key('idx'):
            out['idx'] = 'all'
        else:
            if not isinstance(out['idx'], np.ndarray):
                if isinstance(out['idx'], list):
                    out['idx'] = np.array(out['idx'])
                elif isinstance(out['idx'], int):
                    out['idx'] = np.array([out['idx']])
                elif out['idx'] == 'all':
                    pass
                else:
                    raise TypeError(
                             'The indices of parameter \'%s\' are not ints.'
                             % out['name'])

    def _set_num_output(self):
        """
        Compute the total number of output.
        """
        count_idx = 0
        for out in self.output:
            count_idx += len(out['idx'])
        self._num_output = count_idx * 365

    def _metaget(self, name, idx):
        """
        Get a particular variable called ``name``.
        """
        with Dataset(self.output_file, 'r') as fd:
            if idx == 'all':
                var = fd.variables[name][:]
            else:
                var = fd.variables[name][:, idx]
            if var.shape[0] == 1 or var.shape[1] == 1:
                return var.flatten()
            return var

    def _add_output_attributes(self):
        """
        Adds attributes for all the outputs we have.
        """
        for out in self.output:
            name = out['name']
            idx = out['idx']
            if idx == 'all':
                fget = 'lambda x: x._metaget(\'%s\', \'all\')' % name
            else:
                fget = 'lambda x: x._metaget(\'%s\', np.%s)' % (name,
                                                                repr(idx))
            code = 'setattr(Output, \'%s\', property(fget=%s))' % (name,
                                                                   fget)
            eval(code)

    def _set_output(self, output):
        """
        Sets the output we want to look at according to ``output``.
        """
        if output is None:
            output = self.case.output
        if not isinstance(output, list):
            output = [output]
        for out in output:
            if not isinstance(out, dict):
                out = self._get_dict_from_str(out)
            else:
                self._check_dict(out)
        self._output = output
        self._set_num_output()
        self._add_output_attributes()

    def __init__(self, case, output=[{'name': 'LEAFC', 'idx': 20},
                                     {'name': 'TLAI', 'idx': 20},
                                     {'name': 'ORGANC', 'idx': 20},
                                     {'name': 'STEMC', 'idx': 20},
                                     {'name': 'GPP', 'idx': 20},
                                     {'name': 'NEE', 'idx': 4},
                                     {'name': 'GDDFRAC', 'idx': 20}]):
        """
        Initialize the object.
        """
        if not isinstance(case, Case):
            raise TypeError('\'case\' must be a Case.')
        self._case = case
        self._set_output(output)

    def get_as_array(self):
        """
        Get the outputs as an array of ``num_input`` dimensions.
        """
        res = []
        for out in self.output:
            res.append(getattr(self, out['name']).flatten('F'))
        return np.hstack(res)

    def get_as_dataframe(self):
        """
        Get the outputs as a :class:`pandas.DataFrame`.
        """
        data = []
        columns = []
        for out in self.output:
            column = getattr(self, out['name'])
            if column.ndim == 1:
                column = np.atleast_2d(column).T
                columns.append(out['name'])
            else:
                for i in out['idx']:
                    columns.append(out['name'] + '_' + str(i))
            data.append(column)
        data = np.hstack(data)
        return pd.DataFrame(data, index=np.arange(1, data.shape[0] + 1),
                            columns=columns)

    def get_variable_from_array(self, name, x):
        i = 0
        for out in self.output:
            if not name == out['name']:
                i += 365 * len(out['idx'])
        return x[i:(i + 365)]
