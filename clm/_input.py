"""
.. _input::

==============================
Manipulating the Code's Inputs
==============================

The total number of inputs to CLM-Crop is about 3,000. Obviously we do not
want to have them all around. We would like to be able to fix some of them
and play with the rest. This is achieved via the class :class:`clm.Input`.
However, before we start playing with it, let us discuss the
format of the input files to CLM-Crop.

Here is a simple example::

    >>> import clm
    >>> free_params = [{'name': 'leafcn', 'idx': 20},
    ...                {'name': 'fleafcn', 'idx': 20},
    ...                {'name': 'fstemcn', 'idx': 20},
    ...                {'name': 'organcn', 'idx': 20},
    ...                {'name': 'frootcn', 'idx': 20},
    ...                {'name': 'livewdcn', 'idx': 20}]
    >>> input = clm.Input(case, free_param=free_param)
    >>> print input.leafcn
        25.
    >>> input.leafcn = 30.
    >>> print input.leafcn
        30.

Basicaly, :class:`clm.Input` provides an easy way to explicitly state which of the
variables you would like to be able to change for the simulation. This is achieved
by specifying the ``free_param`` variable. The format of ``free_param`` is explained
in detail in :class:`clm.Input`. You have to specify the name of the input
variable and its dimensios that you would like to be able to change. Then, the
variable is exposed (with the same name) as an attribute of the class
:class:`clm.Input`. To see how many variables you have specified as changable,
you may use
:attr:`clm.Input.num_input`, e.g.::

    >>> print input.num_input
        6

Finally, you may set or get the variables collectively as a 1D array by using
:meth:`clm.Input.get_as_array()` or :meth:`clm.Input.set_from_array()`, respectively.
The order that the variables appear in this array is the same as the order they
appear in :attr:`clm.Input.free_params`.

.. note::

    Any assignement you make to the input variables is directly made to the
    NetCDF file also. So, be careful because you not to corrupt them!

"""

__docformat__ = 'reStructuredText'


__all__ = ['Input']


from netCDF4 import Dataset
import os
import numpy as np
import pandas as pd
from . import Case


class Input(object):

    """
    Represents the input to CLM-crop.

    This class is used to formalize the input to CLM. The user should specify
    a parameter file ``param_file`` from which we read information about the input
    parameters. This should be of course a valid NetCDF file. This is essential.
    Then, the user can specify the using the variable ``free_param`` which of the
    variables (and in particular which dimensions of them) he wishes to observe.
    This is done in the form of a dictionary as described below. The parameters
    are then exposed by the class using their names.

    :param param_file:      The file containing the parameters. You should get it
                            from :meth:`clm.Case.param_file`. The contents of this
                            file can be changed from this object.
    :type param_file:       str
    :param free_param:      An object describing which parameters you consider
                            as free. That is, only these parameters will be allowed
                            to change through this class. This object should be in one
                            of two forms:
                                + a list of strings: Then all the elements of the
                                  list are assumed to be valid parameters. All the
                                  dimensions of these parameters will be available
                                  for change.
                                + a list of dictionaries (one for each variable) you
                                  wish to construct. The dictionary should have the
                                  following fields:
                                    + 'name': the name of the variable (str).
                                    + 'idx': the indices of the variable (int).

                            If ``None``, then all the variables are considered
                            changeable.
    :type free_param:       list of str or list of dict


    """

    # The case
    _case = None

    # A description of the parameters that are allowed to change.
    _free_params = None

    # The total number of inputs that are free to change
    _num_input = None

    @property
    def case(self):
        """
        :getter:        Get the underlying case.
        :type:          str
        """
        return self._case

    @property
    def param_file(self):
        """
        :getter:        Get the parameters file.
        :type:          str
        """
        return self.case.param_file

    @property
    def free_params(self):
        """
        :getter:        Get the description of the parameters that are allowed to
                        change.
        :type:          str
        """
        return self._free_params

    def get_all_variables(self):
        """
        Return all the variables that are in the parameter file.
        """
        with Dataset(self.param_file, 'r') as fd:
            str_vars = [str(key) for key in fd.variables.keys()]
        return str_vars

    def _str_to_dict(self, str_param):
        """
        Turn a parameter from a string to a dictionary.
        """
        with Dataset(self.param_file, 'r') as fd:
            if not fd.variables.has_key(str_param):
                raise ValueError('Parameter \'%s\' could not be found in \'%s\'.'
                                 %(str_param, self.param_file))
            param = dict()
            param['name'] = str_param
            param['idx']  = np.arange(fd.variables[str_param].size)
            param['size'] = fd.variables[str_param].size
        return param

    def _check_param(self, param):
        """
        Check if ``param`` is a valid parameter dictionary.
        """
        if not param.has_key('name'):
            raise ValuError('The parameter dictionary must contain a \'name\'.')
        with Dataset(self.param_file, 'r') as fd:
            if not fd.variables.has_key(param['name']):
                raise ValueError('Parameter \'%s\' could not be found in \'%s\'.'
                                 %(param['name'], self.param_file))
            if not param.has_key('idx'):
                param['idx'] = np.arange(fd.variables[param['name']].size)
            else:
                if not isinstance(param['idx'], np.ndarray):
                    if isinstance(param['idx'], list):
                        param['idx'] = np.array(param['idx'])
                    elif isinstance(param['idx'], int):
                        param['idx'] = np.array([param['idx']])
                    else:
                        raise TypeError(
                                'The indices of parameter \'%s\' are not ints.'
                                % param['name'])
                for i in param['idx']:
                    if not (i >= 0 and i < fd.variables[param['name']].size):
                        raise ValueError('Index out of bounds in parameter \'%s\'.'
                                         % param['name'])
            param['size'] = len(param['idx'])

    def _regularize_free_params(self, free_params):
        """
        Regularize the free parameters so that they are a list of dictionaries.
        """
        new_free_params = []
        for param in free_params:
            if isinstance(param, str):
                new_free_params.append(self._str_to_dict(param))
            elif isinstance(param, dict):
                self._check_param(param)
                new_free_params.append(param)
            else:
                raise TypeError('Parameter \'%s\' is not of a string or a dictionary.'
                                % str(param))
        return new_free_params

    def _metaget(self, name, idx):
        """
        Metaget for parameter ``name`` with indices idx.
        """
        with Dataset(self.param_file, 'r') as fd:
            value = fd.variables[name][idx]
        if len(value) == 1:
            return value[0]
        else:
            return value

    def _metaset(self, name, idx, value):
        """
        Metaset for parameter ``name`` with indices idx.
        """
        if isinstance(value, np.ndarray) and value.ndim == 0:
            value = np.array([float(value)])
        if not isinstance(value, np.ndarray):
            value = np.array([value])
        if not len(value) == len(idx):
            raise ValueError(
        'Attempting to set parameter \'%s\' with dimension \'%d\' using an object'
        % (name, len(idx))
        + ' of dimension \'%d\'.' % len(value))
        with Dataset(self.param_file, 'a') as fd:
            fd.variables[name][idx] = value

    def _fix_properties(self):
        """
        Fix the properties of the class in order to give easy access to the
        parameters.
        """
        for param in self.free_params:
            name = param['name']
            idx = param['idx']
            fget = 'lambda x: Input._metaget(x, \'%s\', np.%s)' % (name, repr(idx))
            fset = 'lambda x, y: Input._metaset(x, \'%s\', np.%s, y)' %(name, repr(idx))
            code = 'setattr(Input, \'%s\', property(fget=%s, fset=%s))' %(name, fget,
                                                                          fset)
            eval(code)

    def _get_num_input(self):
        """
        Get the number of inputs that are free to change.
        """
        num_input = 0
        for param in self.free_params:
            num_input += len(param['idx'])
        return num_input

    def _set_free_params(self, free_params):
        """
        Set the description of the parameters that are allowed to change.
        """
        if free_params is None:
            free_params = self.get_all_variables()
        if not isinstance(free_params, list):
            free_params = [free_params]
        self._free_params = self._regularize_free_params(free_params)
        self._fix_properties()
        self._num_input = self._get_num_input()

    def __init__(self, case, free_params=[{'name': 'leafcn', 'idx': 20},
                                          {'name': 'fleafcn', 'idx': 20},
                                          {'name': 'livewdcn', 'idx': 20},
                                          {'name': 'fstemcn', 'idx': 20},
                                          {'name': 'frootcn', 'idx': 20}]):
        """Initialize the object."""
        if not isinstance(case, Case):
            raise TypeError('\'case\' must be a Case.')
        self._case = case
        self._set_free_params(free_params)

    @property
    def num_input(self):
        """
        Get the total number of free inputs.
        """
        return self._num_input

    def get_as_array(self):
        """
        Get the inputs represented by an array of ``num_input`` dimensions.

        The order they appear is the order you specified in ``free_params``.
        """
        out = []
        for param in self.free_params:
            out.append(getattr(self, param['name']))
        return np.hstack(out)

    def set_from_array(self, value):
        """
        Set the inputs from an array of ``num_input`` dimensions.

        The order they should appear is the order you specified in ``free_params``.
        """
        count = 0
        for param in self.free_params:
            setattr(self, param['name'], value[count:count + len(param['idx'])])
            count += len(param['idx'])

    def get_probabilistic_model(self):
        """
        Return a probalistic model of the inputs.
        """
        model = dict()
        for var in self.free_params:
            if var.has_key('pdf'):
                model[var['name']] = var['pdf']
        return model

    def fill_random(self):
        """
        Fill the random variables using the probabilistic model.
        """
        for var in self.free_params:
            if var.has_key('pdf'):
                setattr(self, var['name'], var['pdf'].rand())

    def get_pdf_dict(self):
        """
        Return a dictionary containing the pdf's of all the input variables.
        """
        out = {}
        for var in self.free_params:
            out[var['name']] = var['pdf']
        return out

    def get_as_dict(self):
        """
        Return a dictionary containing the values of all the inputs.
        """
        out = {}
        for var in self.free_params:
            out[var['name']] = getattr(self, var['name'])
        return out

    def set_from_dict(self, values):
        """
        Set the values of all the inputs from a dictionary.
        """
        for var in self.free_params:
            setattr(self, var['name'], values[var['name']])

    def get_as_dataframe(self):
        """
        Return the input as a :class:`pandas.DataFrame`.
        """
        return pd.DataFrame(np.atleast_2d(self.get_as_array()),
                            columns=[var['name'] for var in self.free_params])
