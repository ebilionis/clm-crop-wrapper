"""

.. _clm_new_case:

We are starting from scratch.
It should be much simpler.
"""


__docformat__ = 'reStructuredText'


__all__ = ['Case']


import os
import shutil
import itertools
import xml.etree.ElementTree as XMLTree
import subprocess
import re
from _misc import replace_namelist_fields
from _misc import get_namelist_fields


class Case(object):

    # The case directory
    _case_dir = None

    """
    The object represents a CLM case.

    Here is how it works:
    + It accepts as an argument a directory containing the exe part of
      a CLM case.
    + This is the directory that contains the directories bld and run.
    + The name of this directory will be associated with the name of
      the case.
    + It is necessary that the case can be run locally without any
      problem.
    + The case can be cloned by simply copying the base directory.
    + The case should know where the input and output files are.
    + The case should know what year it is.
    + To make cases that satisfy the attributes above, the static
      :meth:`Case.create_from_clm_case(case_dir)` should be used.
    """

    def __init__(self, case_dir):
        """
        Initialize the case.
        """
        self.case_dir = case_dir

    @property
    def case_dir(self):
        """
        :getter:    Get the case directory.
        :setter:    Set the case directory.
        :type:      str
        """
        return self._case_dir

    @case_dir.setter
    def case_dir(self, value):
        """
        Set the case directory.
        """
        value = str(value)
        assert os.path.isdir(value), value + ' does not exist!'
        exe_files = ['cesm.exe']
        in_files = ['datm_atm_in',
                    'datm_in',
                    'drv_flds_in',
                    'drv_in',
                    'lnd_in',
                    'rof_in']
        nml_files = ['atm_modelio.nml',
                     'cpl_modelio.nml',
                     'glc_modelio.nml',
                     'ice_modelio.nml',
                     'lnd_modelio.nml',
                     'ocn_modelio.nml',
                     'rof_modelio.nml',
                     'wav_modelio.nml']
        # All these files must exist
        files_to_check = [os.path.join(value, file)
                          for file in exe_files + in_files + nml_files]
        for file in files_to_check:
            assert os.path.exists(file), file + ' does not exist!'
        self._case_dir = value

    @property
    def drv_in(self):
        """
        Get the ``drv_in`` namelist.
        """
        return os.path.join(self.case_dir, 'drv_in')

    @property
    def lnd_in(self):
        """
        Get the ``lnd_in`` namelist.
        """
        return os.path.join(self.case_dir, 'lnd_in')

    @property
    def name(self):
        """
        :getter:    Get the case name.
        :type:      str
        """
        drv_in_fields = get_namelist_fields(self.drv_in,
                                            ['case_name'])
        return drv_in_fields['case_name']

    @property
    def start_ymd(self):
        """
        :getter:    Get the starting date.
        :type:      str
        """
        with open(self.drv_in, 'r') as fd:
            data = fd.read()
        p = re.compile(r'\s+start_ymd\s+=(.*?)\n')
        m = p.search(data)
        if m is None:
            raise RuntimeError('start_ymd not found')
        return m.group(1).strip()

    @property
    def param_file(self):
        """
        :getter:    Get the parameter file.
        :type:      str
        """
        lnd_in_fields = get_namelist_fields(self.lnd_in,
                                            ['paramfile'])
        file = lnd_in_fields['paramfile']
        if os.path.isabs(file):
            return file
        else:
            return os.path.join(self.case_dir, file[2:])

    @property
    def output_file(self):
        """
        :getter:    Get the output file.
        :type:      str
        """
        start_date = self.start_ymd
        #year = start_date[:4]
        year = '0002'
        #month = start_date[4:6]
        month = '01'
        #day = start_date[6:8]
        day = '02'
        basename = '.'.join([self.name, 'clm2', 'h0',
                             '-'.join([year, month, day, '00000']),
                             'nc'])
        return os.path.join(self.case_dir, basename)

    @property
    def rpointer_files(self):
        """
        Get a list containing the rpointer files.
        """
        with open(os.path.join(self.case_dir, 'rpointer_files.txt'), 'r') as fd:
            files = fd.read().splitlines()
        return files

    def _prepare_run(self):
        """
        Does everything that needs to be done before starting a run.
        """
        other_files_to_copy = []
        for file in self.rpointer_files:
            shutil.copy(file, self.case_dir)
            old_case_dir = os.path.split(file)[0]
            with open(file, 'r') as fd:
                other_files_to_copy += [os.path.join(old_case_dir, l.strip())
                                        for l in fd.read().splitlines()]
        other_files_to_copy = set(other_files_to_copy)
        for file in other_files_to_copy:
            try:
                shutil.copy(file, self.case_dir)
            except:
                pass

    def run(self, wait=True):
        """
        Run the case and wait until its done.

        :param wait:    If ``True`` then we wait until the process is done and return
                        nothing. If ``False``, then we simply return the running
                        process.
        :type wait:     bool
        """
        self._prepare_run()
#        from mpi4py import MPI as mpi
#        rank = mpi.COMM_WORLD.Get_rank()
#        size = mpi.COMM_WORLD.Get_size()
        # Get the hex-codes of all processors
#        if rank == 0:
#            proc = subprocess.Popen(['hwloc-distrib', '--single', str(size)],
#                                    stdout=subprocess.PIPE,
#                                    stderr=subprocess.PIPE)
#            proc.wait()
#            if not proc.returncode == 0:
#                raise ValueError('Not able to list available cores.')
#            hex_cores = proc.stdout.read().splitlines()
#        else:
#            hex_cores = None
        # Broadcast this info
#        hex_cores = mpi.COMM_WORLD.bcast(hex_cores)
        run_script_file = os.path.join(self.case_dir, 'run.sh')
#        proc = subprocess.Popen(['hwloc-bind', hex_cores[rank], './cesm.exe'],
        proc = subprocess.Popen(['./cesm.exe'],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                cwd=self.case_dir)
        if wait:
            proc.wait()
            if not proc.returncode == 0:
                raise ValueError('Not able to run case on these initial conditions.')
        else:
            return proc

    def clone(self, new_case_dir):
        """
        Clone the case.

        This should be fairely easy. We just have to copy the directory since
        I have made all the paths to files relative.
        """
        shutil.copytree(self.case_dir, new_case_dir)
        return Case(new_case_dir)

    @staticmethod
    def create_from_clm_case(clm_case_dir, new_case_dir):
        """
        Create a case that can be run independently from anything, using
        a native CLM case.

        :param case_dir:     The exe directory of the case.
        :type case_dir:      str
        :param new_case_dir: The directory where the new case will be stored.
        :type new_case_dir:  str
        """
        # A sanity check: The clm_case_dir must be of the appropriate form
        clm_case_dir = str(clm_case_dir)
        new_case_dir = str(new_case_dir)
        clm_case_dir_run = os.path.join(clm_case_dir, 'run')
        clm_case_dir_bld = os.path.join(clm_case_dir, 'bld')
        dirs_that_must_exist = [clm_case_dir, clm_case_dir_run, clm_case_dir_bld]
        for dir in dirs_that_must_exist:
            assert os.path.isdir(dir), dir + ' does not exist!'
        # Lists of the files we need to copy
        exe_files = ['bld/cesm.exe']
        in_files = ['run/datm_atm_in',
                    'run/datm_in',
                    'run/drv_flds_in',
                    'run/drv_in',
                    'run/lnd_in',
                    'run/rof_in']
        nml_files = ['run/atm_modelio.nml',
                     'run/cpl_modelio.nml',
                     'run/glc_modelio.nml',
                     'run/ice_modelio.nml',
                     'run/lnd_modelio.nml',
                     'run/ocn_modelio.nml',
                     'run/rof_modelio.nml',
                     'run/wav_modelio.nml']
        # Start from scratch
        if os.path.isdir(new_case_dir):
            print 'Removing existing case dir:', new_case_dir
            shutil.rmtree(new_case_dir)
        # Create the new directories
        data_dir = os.path.join(new_case_dir, 'data')
        out_dir = os.path.join(new_case_dir, 'out')
        timing_dir = os.path.join(new_case_dir, 'timing')
        dirs_to_make = [new_case_dir, data_dir, out_dir, timing_dir]
        for dir in dirs_to_make:
            print 'Creating', dir
            os.makedirs(dir)
        # Copy all the files there
        files_to_copy = [os.path.join(clm_case_dir, file)
                         for file in exe_files + in_files + nml_files]
        for file in files_to_copy:
            print 'Copying', file, 'to', new_case_dir
            shutil.copy(file, new_case_dir)
        # Figure out the new local names
        in_files = [os.path.join(new_case_dir, os.path.basename(file))
                    for file in in_files]
        nml_files = [os.path.join(new_case_dir, os.path.basename(file))
                     for file in nml_files]
        # A sanity check: do the new files really exist?
        for file in in_files + nml_files:
            assert os.path.exists(file), file + ' does not exist!'
        # Figure out all the other files to copy
        files_to_copy = []
        where_to_copy = []
        # From DATM_ATM_IN:
        datm_atm_in_file = os.path.join(new_case_dir, 'datm_atm_in')
        datm_atm_in_fields = get_namelist_fields(datm_atm_in_file,
                                                 ['domainfile', 'streams'])
        files_to_copy.append(datm_atm_in_fields['domainfile'])
        where_to_copy.append(data_dir)
        stream_files = [os.path.join(clm_case_dir_run, s.split()[0])
                        for s in datm_atm_in_fields['streams']]
        files_to_copy += stream_files
        where_to_copy += [new_case_dir] * len(stream_files)
        # From the stream fiels (atmospheric and presaero) we just read:
        for stream in stream_files:
            # Copy the file to a temporary directory and make sure it is
            # a proper tree
            tmp_xml_file = os.path.join(new_case_dir, '.tmp_xml_file')
            shutil.copy(stream, tmp_xml_file)
            with open(tmp_xml_file, 'r') as fd:
                xml_content = fd.readlines()
            xml_content.insert(0, '<root_node_314>\n')
            xml_content.append('</root_node_314>\n')
            with open(tmp_xml_file, 'w') as fd:
                fd.write(''.join(xml_content))
            tree = XMLTree.parse(tmp_xml_file)
            data_folder = tree.find('./fieldInfo/filePath').text.strip()
            data_files = [os.path.join(data_folder, file)
                          for file in tree.find('./fieldInfo/fileNames').text.split()]
            files_to_copy += data_files
            where_to_copy += [data_dir] * len(data_files)
        # From DRV_FLDS_IN:
        drv_flds_in_file = os.path.join(new_case_dir, 'drv_flds_in')
        drv_flds_in_fields = get_namelist_fields(drv_flds_in_file,
                                                 ['megan_factors_file'])
        files_to_copy.append(drv_flds_in_fields['megan_factors_file'])
        where_to_copy.append(data_dir)
        # From LND_IN
        lnd_in_file = os.path.join(new_case_dir, 'lnd_in')
        lnd_in_fields = get_namelist_fields(lnd_in_file,
                                            ['fatmlndfrc',
                                             'finidat',
                                             'fsnowaging',
                                             'fsnowoptics',
                                             'fsurdat',
                                             'paramfile',
                                             'stream_fldfilename_ndep',
                                             'stream_fldfilename_popdens',
                                             'stream_fldfilename_lightng'])
        files_to_copy += lnd_in_fields.values()
        where_to_copy += [data_dir] * len(lnd_in_fields)
        # Do copy file files
        for file, dir in itertools.izip(files_to_copy, where_to_copy):
            if not os.path.isdir(dir):
                print 'Creating', dir
                os.makedirs(dir)
            print 'Copying', file, 'to', dir
            shutil.copy(file, dir)
        # Now edit the files to make sure they point to the right data
        # Edit all nml files
        for nml_file in nml_files:
            print 'Editing', nml_file
            replace_namelist_fields(nml_file, {'diro':'./out'})
        # Edit DATM_ATM_IN
        print 'Editing', datm_atm_in_file
        new_domainfile = ('./data/' +
                          os.path.basename(datm_atm_in_fields['domainfile']))
        replace_namelist_fields(datm_atm_in_file,
                                {'domainfile': new_domainfile})
        # Edit DRV_FLDS_IN
        print 'Editing', drv_flds_in_file
        new_megan_factors_file = ('./data/' +
                                  os.path.basename(drv_flds_in_fields['megan_factors_file']))
        replace_namelist_fields(drv_flds_in_file,
                                {'megan_factors_file': new_megan_factors_file})
        # Edit LND_IN
        print 'Editing', lnd_in_file
        for key in lnd_in_fields.keys():
            lnd_in_fields[key] = './data/' + os.path.basename(lnd_in_fields[key])
        replace_namelist_fields(lnd_in_file, lnd_in_fields)
        # Edit the XML files
        for old_stream in stream_files:
            new_stream = os.path.join(new_case_dir,
                                      os.path.basename(old_stream))
            print 'Editing', new_stream
            tmp_xml_file = os.path.join(new_case_dir, '.tmp_xml_file')
            shutil.copy(new_stream, tmp_xml_file)
            with open(tmp_xml_file, 'r') as fd:
                xml_content = fd.readlines()
            xml_content.insert(0, '<root_node_314>\n')
            xml_content.append('</root_node_314>\n')
            with open(tmp_xml_file, 'w') as fd:
                fd.write(''.join(xml_content))
            tree = XMLTree.parse(tmp_xml_file)
            tree.find('./domainInfo/filePath').text = '\n' + ' ' * 12 + './data\n' + ' ' * 12
            tree.find('./fieldInfo/filePath').text = '\n' + ' ' * 12 + './data\n' + ' ' * 12
            tree.write(tmp_xml_file)
            with open(tmp_xml_file, 'r') as fd:
                xml_content = fd.readlines()
            xml_content = xml_content[1:-1]
            with open(tmp_xml_file, 'w') as fd:
                fd.write(''.join(xml_content))
            shutil.copy(tmp_xml_file, new_stream)
            os.remove(tmp_xml_file)
        print 'Writing rpointer_files.txt'
        with open(os.path.join(new_case_dir, 'rpointer_files.txt'), 'w') as fd:
            fd.write(os.path.join(clm_case_dir_run, 'rpointer.atm') + '\n')
            fd.write(os.path.join(clm_case_dir_run, 'rpointer.drv') + '\n')
            fd.write(os.path.join(clm_case_dir_run, 'rpointer.lnd') + '\n')
        return Case(new_case_dir)


if __name__ == '__main__':
    """
    A few examples for dealing with cases.
    """
    clm_case_dir = '/pvfs/ebilionis/exe/Bo1_clmbgc_2004_clone'
    new_case_dir = clm_case_dir + '_localized'
    case = Case.create_from_clm_case(clm_case_dir, new_case_dir)
    #case = Case(new_case_dir)
    #print case.name
    #print case.param_file
    #print case.output_file
    #new_case = case.clone(new_case_dir + '_01')
    #print new_case.param_file
