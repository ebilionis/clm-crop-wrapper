"""

.. _wrapper:

================
CLM-Crop Wrapper
================

The :class:`clm.Wrapper` class implements a simple-to-use wrapper for CLM-Crop.
To use it, you are going to need a :class:`clm.Case` that has already been created,
a list of changeable inputs and a list of outputs::

    >>> import clm
    >>> case = clm.Case('/case/directory')
    >>> free_params = [{'name': 'leafcn', 'idx': 20},
    ...                {'name': 'fleafcn', 'idx': 20},
    ...                {'name': 'fstemcn', 'idx': 20},
    ...                {'name': 'frootcn', 'idx': 20},
    ...                {'name': 'livewdcn', 'idx': 20}]
    >>> output_list = [{'name': 'LEAFC', 'idx': 20},
    ...                {'name': 'TLAI', 'idx': 20},
    ...                {'name': 'ORGANC', 'idx': 20},
    ...                {'name': 'STEMC', 'idx': 20},
    ...                {'name': 'GPP', 'idx': 20},
    ...                {'name': 'NEE', 'idx': 'all'}]
    >>> f = clm.Wrapper(case, free_params=free_params, output=output_list)

There are several ways to use a wrapper. For example, you can change the inputs
as you wish by accessing them through :attr:`clm.Wrapper.input` and then simply
submit a job using :meth:`clm.Wrapper.submit()`. You can see the PBS id of your
job by accessing :attr:`clm.Wrapper.current_job_id`. Then you can just wait for
the job to finish by calling :meth:`clm.Wrapper.wait()` and finally access the
output via :attr:`clm.Wrapper.output`. This is one way of doing it.

Another way of doing it is to use :class:`clm.Wrapper` as a simple function.
Let's say that you have your input represented as a 1D array
(see :meth:`clm.Input.get_as_array()) named ``x``. Then you can simply get the
output in a 1D array form (see :meth:`clm.Output.get_as_array()`), say ``y``, by
doing::

    >>> y = f(x)

Old job ids can be seen by asking for :attr:`clm.Wrapper.old_job_ids`.


"""


__docformat__ = 'reStructuredText'


__all__ = ['Wrapper']


from netCDF4 import Dataset
import numpy as np
from . import Input
from . import Case
from . import Output
import pymc as pm


class Wrapper(object):

    """
    Represents a wrapper for CLM-crop.

    :param case:        A case that has already been compiled.
    :type case:         :class:`clm.Case`
    :param free_params: The input parameters that you consider as changable.
                        The format is described in :ref:`input` and in
                        :class:`clm.Input`. This is actually used to construct
                        a :class:`clm.Input` object internally accessible via
                        :attr:`clm.Wrapper.input`.
    :type free_params:  list
    :param output:      The output parameters you would like to look at.
                        The format is described in :ref:`output` and in
                        :class:`clm.Output`. This is actually  used to construct
                        a :class:`clm.Output` object internally accessible via
                        :attr:`clm.Wrapper.output`.
    :type output:       list

    """

    # The object representing the input variables.
    _input = None

    # The object representing the case.
    _case = None

    # The number of outputs
    _num_output = None

    @property
    def input(self):
        """
        :getter:    Get the input object.
        :type:      :class:`clm.Input`
        """
        return self._input

    @property
    def case(self):
        """
        :getter:    Get the case.
        :type:      :class:`clm.Case`
        """
        return self._case

    @property
    def output(self):
        """
        :getter:    Get the output object.
        :type:      :class:`clm.Output`
        """
        return self._output

    @property
    def num_input(self):
        """
        :getter:    Get the number of inputs of this function.
        :type:      int
        """
        return self._input.num_input

    @property
    def num_output(self):
        """
        :getter:    Get the number of outputs of this function.
        :type:      int
        """
        return self._output.num_output

    def __init__(self, case,
                       free_params=[{'name': 'leafcn', 'idx': 23,
                                     'pdf': pm.Normal('leafcn', 25., tau=1. / 3.90625)},
                                    {'name': 'fleafcn', 'idx': 23,
                                     'pdf': pm.Normal('fleafcn', 65., tau=1. / 26.4063)},
                                    {'name': 'livewdcn', 'idx': 23,
                                     'pdf': pm.Normal('livewdcn', mu=50., tau=1. / 15.625)},
                                    {'name': 'fstemcn', 'idx': 23,
                                     'pdf': pm.Normal('fstemcn', mu=130., tau=1. / 105.625)},
                                    {'name': 'frootcn', 'idx': 23,
                                     'pdf': pm.Normal('frootcn', mu=42., tau=1. / 11.025)},
                                    {'name': 'graincn', 'idx': 23,
                                     'pdf': pm.Normal('graincn', mu=60., tau=1. / 22.5)}],
                       output=[{'name': 'LEAFC', 'idx': 23},
                               {'name': 'TLAI', 'idx': 23},
                               {'name': 'GRAINC', 'idx': 23},
                               {'name': 'LIVESTEMC', 'idx': 23},
                               {'name': 'GPP', 'idx': 23},
                               {'name': 'NEE', 'idx': 9},
                               {'name': 'GDDPLANT', 'idx': 23},
                               {'name': 'GDDHARV', 'idx': 23}]):
        """
        Initialize the object.
        """
        if not isinstance(case, Case):
            raise TypeError('The \'case\' variable must be a Case object.')
        self._case = case
        self._input = Input(case, free_params=free_params)
        self._output = Output(case, output=output)

    def run(self, wait=True):
        """
        Just run the case with the current input parameters.
        """
        return self.case.run(wait=wait)

    def __call__(self, x):
        """
        Evaluate CLM-crop at ``x``.

        :param x:       The inputs to CLM-Crop.
        :type x:        1D numpy array.
        :returns:       1D numpy array representing the outputs.
        """
        x = np.array(x)
        if not x.ndim == 1:
            raise ValueError('The numpy array should be one dimensional.')
        if not x.shape[0] == self.num_input:
            raise ValueError('Wrong number of input dimensions.')
        self.input.set_from_array(x)
        self.run()
        return self.output.get_as_array()
