"""
Various routines that do not fit anywhere else.

"""


__docformat__ = 'reStructuredText'


__all__ = ['get_terminal_size', 'get_terminal_height', 'get_terminal_width',
           'wait_for_jobs', 'job_is_running', 'jobs_are_running',
           'get_namelist_fields', 'replace_namelist_fields',
           'get_moving_average', 'DAYS_PER_MONTH', 'find_month_of_day',
           'find_index_data_exceeds_value', 'get_slope_at_index',
           'append_data']


import os
import subprocess
import time
import re
import pandas as pd
import numpy as np


def get_terminal_size(fd=1):
    """
    Return height and width of current terminal. First tries to get
    size via termios.TIOCGWINSZ, then from environment. Defaults to 25
    lines x 80 columns if both methods fail.

    :param fd: file descriptor (default: 1=stdout)
    """
    try:
        import fcntl, termios, struct
        hw = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
    except:
        try:
            hw = (os.environ['LINES'], os.environ['COLUMNS'])
        except:
            hw = (25, 80)

    return hw


def get_terminal_height(fd=1):
    """
    Return height of terminal if it is a tty, 999 otherwise

    :param fd: file descriptor (default: 1=stdout)
    """
    if os.isatty(fd):
        height = get_terminal_size(fd)[0]
    else:
        height = 999

    return height


def get_terminal_width(fd=1):
    """
    Return width of terminal if it is a tty, 999 otherwise

    :param fd: file descriptor (default: 1=stdout)
    """
    if os.isatty(fd):
        width = get_terminal_size(fd)[1]
    else:
        width = 999

    return width


def job_is_running(job_id):
    """
    Check if a PBS job is running.

    :param job_id:  The id of the job you want to check.
    :type job_id:   str
    :raises: exc.TypeError
    """
    if not isinstance(job_id, str):
        raise TypeError('The job id(s) must be strings.')
    proc = subprocess.Popen(['qstat', job_id],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    proc.wait()
    return proc.returncode == 0


def jobs_are_running(job_ids):
    """
    Check if a list of PBS jobs are still running.

    :param job_ids:     The id(s) of the jobs you want to check.
    :type job_ids:      str or list of str.
    :raises: exc.TypeError
    """
    if not isinstance(job_ids, list):
        job_ids = [job_ids]
    for job_id in job_ids:
        if job_is_running(job_id):
            return True
    return False


def wait_for_jobs(job_ids, delay=1):
    """
    Wait for PBS jobs to finish.

    :param job_ids:     The id(s) of the jobs you want to wait for.
    :type job_ids:      str or list of str.
    :param delay:       Specifies how often (in seconds) we should check for
                        the job status.
    :type delay:        float
    """
    while jobs_are_running(job_ids):
        time.sleep(delay)


def get_namelist_fields(nml_file, fields):
    """
    Get the values of the fields in a namelist.

    .. note::

        Currently, it can only read fields whose values are simple strings.

    :param nml_file:    The namelist file.
    :type nml_file:     str
    :param fields:      A list of strings describing the fields of the
                        namelist to get.
    :type fields:       list
    :returns:           A dictionary.
    """
    if not isinstance(fields, list):
        fields = [fields]
    for field in fields:
        if not isinstance(field, str):
            raise TypeError('The fields of the namelist must be strings.')
    with open(nml_file, 'r') as fd:
        data = fd.read()
    nml = {}
    for field in fields:
        p = re.compile(r'\s+\b%s\b\s+=\s+(\'|\")(.*?)\1' % field)
        m = p.search(data)
        if m is None:
            raise RuntimeError('The namelist does not contain a field called'
                               ' \'%s\'.' % field)
        nml[field] = m.group(2).strip()
        # Is there at least one more field?
        p = re.compile(r'\s+\b%s\b\s+=\s+(\'|\").*?\1,\s*\1(.*?)\1' % field)
        m = p.search(data)
        if m is not None:
            nml[field] = [nml[field], m.group(2).strip()]
    return nml


def replace_namelist_fields(nml_file, nml):
    """
    Replace the values of the fields in the namelist with those provided in ``nml``.

    .. note::

        Currently, it can only write fields whose values are simple strings.

    :param nml_file:    The namelist file.
    :type nml_file:     str
    :param nml:         A dictionary whose keys are the fields and whose values
                        are the values you want.
    :type nml:          dict
    """
    if not isinstance(nml, dict):
        raise TypeError('The fields ``nml`` have to be specified through a'
                        ' dictionary.')
    with open(nml_file, 'r') as fd:
        data = fd.read()
    for key in nml.keys():
        if not isinstance(key, str):
            raise TypeError('The keys of the namelist dictionary must be'
                            ' strings.')
        val = nml[key]
        if not isinstance(val, str):
            raise TypeError('The value of the key \'%s\'' % key
                            + ' is not a string.')
        p = re.compile(r'(\s+\b%s\b\s+=\s+)(\'|\").*?\2' % key)
        m = p.search(data)
        if m is None:
            raise RuntimeError('The namelist does not contain a field called'
                               + ' \'%s\'.' % key)
        data = p.sub(r"\1\2%s\2" % val, data)
    with open(nml_file, 'w') as fd:
        fd.write(data)


def get_moving_average(series, window):
    """
    :param series:      The time series to average.
    :type series:       1D numpy.ndarray
    :param window:      The window size
    :type window:       int
    """
    # Repeat the last value window / 2 times
    augmented_series = np.hstack([series, np.ones(window / 2) * series[-1]])
    return pd.rolling_mean(augmented_series, window=window, center=True,
                           min_periods=0)[:series.shape[0]]


DAYS_PER_MONTH = np.array([31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31])
"""
The days per month as counted by CESM.

See `What calendars are supported in CESM <http://www.cesm.ucar.edu/models/cesm1.2/cesm/doc/usersguide/x1905.html>`_
"""


def find_month_of_day(day):
    """
    Find the month corresponding to ``day`` (0 to 365 exclusive).

    :param day: The day whose month you want to know. This must be an integer
                between 0 (inclusive) and 365 exclusive. If it is greater than
                365, then we look at ``day % 365``.
    :returns:   The math number (1 to 12 inclusive).
    :rtype:     int
    """
    day = day % np.sum(DAYS_PER_MONTH)
    cum_sum_of_days = np.cumsum(DAYS_PER_MONTH)
    for i in xrange(12):
        if day <= cum_sum_of_days[i]:
            return i + 1
    raise RuntimeError('I cannot associate a month to day: %d.\n' % day)


def find_index_data_exceeds_value(data, value):
    """
    Return the index ``i`` for which ``data[i] >= value`` for the first time.
    """
    for i in xrange(len(data)):
        if data[i] >= value:
            return i
    raise RuntimeError('This never happens.\n')


def get_slope_at_index(data, i, window):
    """
    Get the slope of the data at index ``i``.

    Calculate the slope of the data at index ``i`` by constructing a linear model
    for the data about ``i`` (``window / 2`` to the left and right).
    """
    x = np.arange(max(i - window / 2, 0), min(i + window / 2, len(data)))
    y = data[x]
    return np.polyfit(x, y, 1)


def append_data(inputs, outputs, store, max_digits=9):
    """
    Append the ``inputs`` and ``outputs`` to the ``store``.

    :param inputs:      The inputs.
    :type inputs:       :class:`pandas.DataFrame`
    :param store:       The HDF5 filehandler.
    :type store:        :class:`pandas.HDF5Store`
    :param max_digits:  The maximum number of digits for the name of
                        the output records. If you are planning to store
                        more than a bilion outputs, change this.
    :type max_digits:   int
    """
    if isinstance(inputs, list):
        for input in inputs:
            if not isinstance(input, pd.DataFrame):
                raise TypeError('The inputs must be a list of pandas.DataFrame.')
        inputs = pd.concat(inputs, ignore_index=True)
    if not isinstance(outputs, list):
        outputs = [outputs]
    for out in outputs:
        if not isinstance(out, pd.DataFrame):
            raise TypeError('All outputs must be pandas.DataFrame.')
    if not isinstance(inputs, pd.DataFrame):
        raise TypeError('The inputs must be a pandas.DataFrame.')
    if not inputs.shape[0] == len(outputs):
        raise RuntimeError('The number of inputs and outputs does not match.')
    if '/input' in store.keys():
        # This is an append
        input_df = store['input']
        new_input_df = input_df.append(inputs, ignore_index=True)
        store['input'] = new_input_df
        sample_count = input_df.shape[0]
    else:
        # This is the first time we write data
        store['input'] = inputs
        sample_count = 0
    for out in outputs:
        store['output/s_' + str(sample_count).zfill(max_digits)] = out
        sample_count += 1
