"""

.. _unittest_new_case:

++++++++++++++++++++++++++++++++++++
A unittest for :class:`clm.NewCase`.
++++++++++++++++++++++++++++++++++++

Make sure everything works fine with :class:`clm.NewCase` by running
this test.

"""


import unittest
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import clm
import pandas as pd
import matplotlib.pyplot as plt
import pymc as pm
import numpy as np


class TestNewCase(unittest.TestCase):

    def test_create_1(self):
        for i in range(1, 10):
            print i
            c = clm.Case('/pvfs/ebilionis/exe/localized_cases/Bo1_clmbgc_2004_clone_localized_'
                         + str(i))
            f = clm.Wrapper(c)
            f.run()
            print f.output.get_as_dataframe()
        return
        c = clm.Case('/pvfs/ebilionis/exe/Bo1_clmbgc_2004_clone_localized')
        import pymc as pm
        input = pm.Normal('input',
                      mu=np.array([25., 65., 50., 130., 42.]),
                      tau=np.array([1. / 3.90625, 1. / 26.4063,
                                         1. / 15.625, 1. / 105.625,
                                         1. / 11.025]))
        f = clm.Wrapper(c)
        x = f.input.get_as_array()
        #y = f(x)
        print 'init:', x
        print 'rand:', input.value
        #f.input.set_from_array(input.value)
        df = f.output.get_as_dataframe()
        df.GDDHARV.plot()
        plt.show()
        print 'out:', f.output.get_as_dataframe()
        return
        store = pd.HDFStore('samples.h5')
        clm.append_data(f.input.get_as_dataframe(),
                        f.output.get_as_dataframe(),
                        store)
        for i in range(3):
            clm.append_data(f.input.get_as_dataframe(),
                            f.output.get_as_dataframe(),
                            store)
        print store
        return
        #y = f(x)
        #print y
        plt.plot(f.output.ORGANC)
        plt.show()
        return
        output = clm.Output(c)
        print output.output_file
        print output.NEE
        print output.get_as_array()
        print output.num_output
        return
        id = nc.submit()
        clm.wait_for_jobs(id, verbose=1)
        return
        quit()
        free_params = [ {'name': 'leafcn', 'idx': 20},
                        {'name': 'fleafcn', 'idx': 20},
                        {'name': 'fstemcn', 'idx': 20},
                        {'name': 'organcn', 'idx': 20},
                        {'name': 'livewdcn', 'idx': 20}
                      ]
        input = clm.Input(param_file='pft-physiology_crop_orig.nc',
                          free_params=free_params)
        print input.free_params
        print input.leafcn
        old_leafcn = input.leafcn
#        input.leafcn = 40.
        print input.leafcn
#        input.leafcn = old_leafcn
        print input.num_input
        print input.get_as_array()
        x = input.get_as_array()
        x[0] = 10.
        input.set_from_array(x)

if __name__ == '__main__':
    unittest.main()
