"""
Collect some data to see what is going on here...
"""

import warnings
warnings.filterwarnings('ignore')
import mpi4py.MPI as mpi
import lognormal_prior_model as prior_model
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import cPickle as pickle
import numpy as np
import clm
from optparse import OptionParser


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-s', '--num-samples', dest='num_samples',
                      help='Set the total number of samples to be taken.',
                      type='int')
    parser.add_option('-c', '--case', dest='case',
                      help='Set the case you want to use for taking samples.',
                      type='string')
    parser.add_option('-p', '--prior', dest='prior',
                      help='Set the prior probability model (should be a file'
                           'containing a python module',
                      type='string')

    rank = mpi.COMM_WORLD.Get_rank()
    size = mpi.COMM_WORLD.Get_size()

    num_samples = int(sys.argv[1])
    case_name = 'test_serial'
    p = prior_model.make_model()

    my_samples = num_samples / size

    # Create base case (assume that it is compiled)
    if rank == 0:
        base_case = clm.Case(name=case_name, start_date=start_date)
    else:
        base_case = None
    # Let everyone know about it:
    base_case = mpi.COMM_WORLD.bcast(base_case)

    # Create a clone to be used by this process
    my_case = base_case.clone(case_name + '_' + str(rank))

    # Create a CLM wrapper (the default one)
    f = clm.Wrapper(my_case)

    # Lists that collect inputs and outputs
    my_X = []
    my_Y = []

    # Sample as many times as required
    for i in xrange(my_samples):
        # Draw a random input:
        x = np.array([float(p[v['name']].rand()) for v in f.input.free_params])
        y = f(x)
        my_X.append(x)
        my_Y.append(y)
        print 'samples taken:', (i + 1) * size
    X = np.vstack(mpi.COMM_WORLD.allgather(my_X))
    Y = np.vstack(mpi.COMM_WORLD.allgather(my_Y))
    if rank == 0:
        out_file = 'clm_data_k=%d_s=%d.pickle' % (f.num_input, num_samples)
        with open(out_file, 'wb') as fd:
            pickle.dump((X, Y), fd, pickle.HIGHEST_PROTOCOL)
