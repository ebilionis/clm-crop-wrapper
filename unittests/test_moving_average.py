"""
Compare if the moving average we compute is the same as Zeng
and figure out exactly what she computes.
"""

import numpy as np
import cPickle as pickle
import imp
import matplotlib.pyplot as plt
import pandas as pd


# This is Zengs code:
from zengs_code.MetrHas import MovAve as zengs_moving_average


# This pandas code that does the exact same thing
def moving_average(series, window):
    """
    :param series:      The time series to average.
    :type series:       1D numpy.ndarray
    :param window:      The window size
    :type window:       int
    """
    # Repeat the last value window / 2 times
    augmented_series = np.hstack([series, np.ones(window / 2) * series[-1]])
    return pd.rolling_mean(augmented_series, window=window, center=True,
                           min_periods=0)[:series.shape[0]]


if __name__ == '__main__':
    # Load the data
    with open('clm_data_k=5_s=16.pickle', 'rb') as fd:
        data = pickle.load(fd)
    X, Y = data
    Y = np.vstack(Y)
    GPP = Y[0, 4 * 365:5 * 365]
    plt.plot(GPP, 'b', linewidth=2)
    # Notice that Zeng's window size variable actually corresponds
    # to half the window size:
    GPP_zeng = zengs_moving_average(np.copy(GPP), 15)
    # This is the same calculation using pandas:
    GPP_pd = moving_average(GPP, 30)
    plt.plot(GPP_zeng, 'r', linewidth=2)
    plt.plot(GPP_pd, 'g', linewidth=2)
    plt.legend(['Zeng\'s moving average', 'Pandas moving average'])
    plt.show()
