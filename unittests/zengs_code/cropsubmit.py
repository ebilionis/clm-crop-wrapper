import os, sys, csv,string

start=7800

end=8000

step=20


i=start/step+1
filename='cropbatchrun'+str(i)+'.sh'
myfile=open(filename,'w') 
m=start
n=m+step
 
myfile.write('#!/bin/tcsh \n') 
myfile.write('#===============================================================================\n')
myfile.write('# FUSION_USER\n') 
myfile.write('# This is where the batch submission is set.  The above code computes\n')
myfile.write('# the total number of tasks, nodes, and other things that can be useful\n') 
myfile.write('# here.  Use PBS, BSUB, or whatever the local environment supports.\n') 
myfile.write('#===============================================================================\n') 
myfile.write('#PBS -A UQCLM\n') 
myfile.write('#PBS -N HM4_'+str(i)+'\n') 
myfile.write('#PBS -l nodes=1:ppn=8\n')
myfile.write('#PBS -l walltime=0:59:59\n') 
myfile.write('#PBS -j oe\n') 
#myfile.write('#PBS -q shared\n')
myfile.write('#PBS -W depend=afterok:1983102.fmgt2.lcrc.anl.gov')
myfile.write('\n') 
myfile.write('#limit coredumpsize 1000000\n') 
myfile.write('#limit stacksize unlimited\n')
myfile.write('\n')
myfile.write('echo "`date` -- Tasks EXECUTION BEGINS HERE"\n') 
myfile.write('cd /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase\n')
myfile.write('\n') 
myfile.write('python /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/EffiMH_obs_slope.py --end '+str(n)+' --start '+ str(m)+ ' --filenumber '+ str(i)+'\n')
#myfile.write('mpirun -n 16 a.out\n')
myfile.write('\n')
myfile.write('wait\n')
myfile.write('\n')
myfile.write('echo "`date` -- Tasks EXECUTION END HERE"\n')
myfile.write('#---------------------------end  -----------------------------\n')


myfile.close()


os.system('qsub '+filename + ' > wrfQsub.out')
temp = open('wrfQsub.out', 'r')
wrf_id = temp.readline()

while n<end:
    i=i+1
    filename='cropbatchrun'+str(i)+'.sh'
    myfile=open(filename,'w')
    m=n
    n=m+step

    myfile.write('#!/bin/tcsh \n')
    myfile.write('#===============================================================================\n')
    myfile.write('# FUSION_USER\n')
    myfile.write('# This is where the batch submission is set.  The above code computes\n')
    myfile.write('# the total number of tasks, nodes, and other things that can be useful\n')
    myfile.write('# here.  Use PBS, BSUB, or whatever the local environment supports.\n')
    myfile.write('#===============================================================================\n')
    myfile.write('#PBS -A UQCLM\n')
    myfile.write('#PBS -N HM4_'+str(i)+'\n')
    myfile.write('#PBS -l nodes=1:ppn=8\n')
    myfile.write('#PBS -l walltime=0:59:59\n')
    myfile.write('#PBS -j oe\n')
    #myfile.write('#PBS -q shared\n')
    myfile.write('#PBS -W depend=afterok:'+wrf_id+'\n')
    myfile.write('\n')
    myfile.write('#limit coredumpsize 1000000\n')
    myfile.write('#limit stacksize unlimited\n')
    myfile.write('\n')
    myfile.write('echo "`date` -- Tasks EXECUTION BEGINS HERE"\n')
    myfile.write('cd /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase\n')
    myfile.write('\n')
    myfile.write('python /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/EffiMH_obs_slope.py --end '+str(n)+' --start '+ str(m)+ ' --filenumber '+ str(i)+'\n')
    #myfile.write('mpirun -n 16 a.out\n')
    myfile.write('\n')
    myfile.write('wait\n')
    myfile.write('\n')
    myfile.write('echo "`date` -- Tasks EXECUTION END HERE"\n')
    myfile.write('#---------------------------end  -----------------------------\n')
    myfile.close()

    os.system('qsub '+filename  + ' >  wrfQsub.out')
    temp = open('wrfQsub.out', 'r')
    wrf_id = temp.readline()
 

