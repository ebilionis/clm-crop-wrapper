import os, sys, csv,string
sys.path.append("/home/zeng/bin/lib/python2.6/site-packages/Numeric")
from Numeric import *
from RandomArray import *
from netCDF4 import Dataset

def wrapper(func, *args): 
   return func(*args) 
"""

def MHfun(initial,pm,pv, testpdf,n,burn,thin):
   dim=initial.shape[0];
   #x = zeros((1,dim)) # initial guess
   m=n*thin + burn
   vec = zeros((n+1,dim))*0.1
   #normalmean=[0,0]
   #normalcov=[[1,0],[0,1]]
   #normalmean=zeros((dim,))
   normalmean=pm
   normalcov=identity(dim)
   for i in range(0,dim):
       normalcov[i][i]=pv[i] 
   print normalcov
   innov=multivariate_normal(normalmean,normalcov,m)  # proposal density
   #print innov.shape
   naccept = 0
   x=initial
   #print vec[0].shape,x.shape
   vec[0]=x 
   keep=1
   oldpdf=wrapper(testpdf,x)
   for i in range(2,m+1):
      can = x + innov[i-1] #candidate
      newpdf=wrapper(testpdf,can)
      
      aprob = min([1.,newpdf/oldpdf]) #acceptance probability
      u = uniform(0,1)
      if u < aprob:
          x = can
          oldpdf=newpdf
          if (i > burn) &( (i%thin)==0):
             naccept = naccept +1
      if (i > burn) & ((i%thin)==0):
          vec[keep]=x
          keep=keep+1
      print x, newpdf, oldpdf, "\n" 
   acceptRatio = float(naccept)/(float(keep-1))
   return vec, acceptRatio



def MHfun_log(initial,pm,pv, testpdf,n,burn,thin):
   dim=initial.shape[0];
   #x = zeros((1,dim)) # initial guess
   m=n*thin + burn
   vec = zeros((n+1,dim))*0.1
   #normalmean=[0,0]
   #normalcov=[[1,0],[0,1]]
   #normalmean=zeros((dim,))
   normalmean=pm
   normalcov=identity(dim)
   for i in range(0,dim):
       normalcov[i][i]=pv[i]
   print normalcov
   innov=multivariate_normal(normalmean,normalcov,m)  # proposal density
   #print innov.shape
   naccept = 0
   x=initial
   #print vec[0].shape,x.shape
   vec[0]=x
   keep=1
   oldlogpdf=wrapper(testpdf,x)
   for i in range(2,m+1):
      can = x + innov[i-1] #candidate
      newlogpdf=wrapper(testpdf,can)
      
      #aprob = min([1.,exp(newlogpdf-oldlogpdf)]) #acceptance probability
      logaprob = min([0.,(newlogpdf-oldlogpdf)])
      u = uniform(0,1)
      logu=log(u) 
      #if u < aprob:
      if logu<logaprob:
          x = can
          oldlogpdf=newlogpdf
          if (i > burn) &( (i%thin)==0):
             naccept = naccept +1
      if (i > burn) & ((i%thin)==0):
          vec[keep]=x
          keep=keep+1
      print x, newlogpdf, oldlogpdf, "\n"
   acceptRatio = float(naccept)/(float(keep-1))
   return vec, acceptRatio

def MYMHfun(initial,pm,pv, testpdf, obs,Osigma,Vsigma,Vmean,n,burn,thin):
   dim=initial.shape[0];
   print dim
   #x = zeros((1,dim)) # initial guess
   m=n*thin + burn
   vec = zeros((n+1,dim))*0.1
   
   normalmean=pm
   #normalcov=[[0.0010000,0,0,0,0,0],[0,0.0010000,0,0,0,0],[0,0,0.0010000,0,0,0],[0,0,0,0.0010000,0,0],[0,0,0,0,0.0010000,0],[0,0,0,0,0,0.0010000]]
   normalcov=identity(dim)
   for i in range(0,dim):
       normalcov[i][i]=pv[i]
   print normalcov
   innov=multivariate_normal(normalmean,normalcov,m)  # proposal density
   print innov.shape
   naccept = 0
   x=initial
   #print vec[0].shape,x.shape
   vec[0]=x
   keep=1
   oldpdf=wrapper(testpdf,x,obs,Osigma,Vsigma,Vmean)
   for i in range(2,m+1):
      can = x + innov[i-1] #candidate
      print i, exp(can)
      newpdf=wrapper(testpdf,can,obs,Osigma,Vsigma,Vmean)

      aprob = min([1.,newpdf/oldpdf]) #acceptance probability
      u = uniform(0,1)
      if u < aprob:
          x = can
          oldpdf=newpdf
          if (i > burn) &( (i%thin)==0):
             naccept = naccept +1
      if (i > burn) & ((i%thin)==0):
          vec[keep]=x
          keep=keep+1
   acceptRatio = float(naccept)/(float(keep-1))
   return vec, acceptRatio


   
def MYMHfun_log(initial,pm,pv, testpdf, obs,Osigma,Vsigma,Vmean,n,burn,thin):
   dim=initial.shape[0];
   print dim
   #x = zeros((1,dim)) # initial guess
   m=n*thin + burn
   vec = zeros((n+1,dim))*0.1
   
   normalmean=pm
   #normalcov=[[0.0010000,0,0,0,0,0],[0,0.0010000,0,0,0,0],[0,0,0.0010000,0,0,0],[0,0,0,0.0010000,0,0],[0,0,0,0,0.0010000,0],[0,0,0,0,0,0.0010000]]
   normalcov=identity(dim)*0.1
   for i in range(0,dim):
       normalcov[i][i]=pv[i]
       print i,pv[i]
   print normalcov,pm
   innov=multivariate_normal(normalmean,normalcov,m)  # proposal density
   print innov.shape
   naccept = 0
   x=initial 
   #print vec[0].shape,x.shape
   vec[0]=x
   keep=1
   oldlogpdf=wrapper(testpdf,x,obs,Osigma,Vsigma,Vmean)
   for i in range(2,m+1):
      can = x + innov[i-1] #candidate
      print i,can,"\n", innov[i-1], "\n",exp(can)
      newlogpdf=wrapper(testpdf,can,obs,Osigma,Vsigma,Vmean)

      #aprob = min([1.,exp(newlogpdf-oldlogpdf)]) #acceptance probability
      logaprob = min([0.,(newlogpdf-oldlogpdf)])
      u = uniform(0,1)
      logu=log(u)
      #if u < aprob:
      if logu<logaprob:
          x = can
          oldlogpdf=newlogpdf
          if (i > burn) &( (i%thin)==0):
             naccept = naccept +1
      if (i > burn) & ((i%thin)==0):
          vec[keep]=x
          keep=keep+1
   acceptRatio = float(naccept)/(float(keep-1))
   return vec, acceptRatio


"""
def readdata(doy,pft,varstr,file):
  #filedir='/fusion/gpfs/home/zeng/exe/PARA_1_US-Bo1_I_2000_CN/run'
  #ncfile='PARA_1_US-Bo1_I_2000_CN.clm2.h1.0501-01-01-00000.nc'
  a=file.rpartition("/")
  filedir=a[0]
  ncfile=a[2]
  os.chdir(filedir)
  #DOY= array([146, 153,160,167,174,181,188,195,202,209,216,223,230,244,251])
  #varstr=['LEAFC','STEMC','ORGANC']
  nc=Dataset(ncfile, 'r')
  i=0;
  dim=len(varstr) 
  num=len(doy)
  z=ones((dim,num))*0.1 
  for v in varstr:
        myvar=nc.variables[v]
        j=0
        for l in doy:
          
          if pft[i][0]>=0:
             #print i,pft[i][0]
             z[i][j]=myvar[l-1][pft[i][0]]
          else:
             z[i][j]=myvar[l-1] 
          j=j+1
        i=i+1

  nc.close()
  return z


def changevar(z,pft,str,file):
  #filedir='/fusion/gpfs/home/zeng'
  #ncfile='pft-physiology_crop70.c110307.nc'
  #str=['leafcn','fleafcn','fstemcn','organcn','frootcn','ffrootcn','livewdcn']
  a=file.rpartition("/")
  filedir=a[0]
  ncfile=a[2]
  os.chdir(filedir)
  nc = Dataset(ncfile, 'a')
  i=0;
  for v in str:
     myvar=nc.variables[v]
     myvar[pft[i][0]]=z[i]
     i=i+1
  nc.close()
  return 1 


def Slope_Month(frac,outfile):
 myvar=['GDDFRAC']
 mydoy=range(365) 
 mypft=20*ones((1,1))
 SS=readdata(mydoy,mypft,myvar,outfile)
 gddfrac=SS[0] 
 a=gddfrac[0]; 
 i=0 
 while a<frac:
   i=i+1
   a=gddfrac[i]
  
 b=[30,59,90,120,151,181,212,243,273,304,334,365]
 if i<b[0]:
   mymonth =1 
 elif i<b[1]: 
   mymonth=2 
 elif i<b[2]:
   mymonth=3 
 elif i<b[3]: 
   mymonth=4 
 elif i<b[4]: 
   mymonth=5 
 elif i<b[5]: 
   mymonth=6 
 elif i<b[6]:
   mymonth=7 
 elif i<b[7]: 
   mymonth=8  
 elif i<b[8]:
   mymonth=9
 elif i<b[9]:
   mymonth=10 
 elif i<b[10]:
   mymonth=11
 elif i<b[11]:
   mymonth=12

 myday =i
 return myday,mymonth

def Slope1(var,outfile,pft):
 myvar=var
 myvar.append('GDDFRAC') 
 d=len(var)
 mydoy=range(365)
 mypft=zeros((d,1))
 slope=zeros((d-1,),Float)
 for i in range(d-1):
   mypft[i][0]=pft[i][0]
 mypft[d-1][0]=20
 S=readdata(mydoy,mypft,myvar,outfile)
 gddfrac=S[d-1]
 a=gddfrac[0]; 
 i=0 
 while a<0.3:
   a=gddfrac[i]
   i=i+1
 for j in range(d-1):
   b=S[j][i]
   c=S[j][i+20]
   slope[j]=(c-b)/20.0  
 return slope
 
def Slope1_1(S,outfile):
 d=S.shape[0]
 #print S.shape
 slope=zeros((d,),Float)
 i,k= Slope_Month(0.3,outfile)
 for j in range(d):
   #print j, i
   b=S[j][i]
   c=S[j][i+20]
   #print b,c
   slope[j]=(c-b)/20.0  
 return slope



def Slope2(obs,obsdoy):
 d=obs.shape[0]
 slope=zeros((d,),Float)
 for i in range(d):
   s=obs[i]
   j=0
   while s[j]<=0:
        j=j+1     
   j1=j
   c=max(s)
   while s[j]<c:
        j=j+1
   j2=j
   slope[i]=(obs[i][j2]-obs[i][j1])/(obsdoy[j2]-obsdoy[j1])
 return slope


def MovAve(S,hlfwin):
  len=S.shape[0]
  il=len-1
  Sma=zeros((len,),Float)
  Sma[0]=S[0]
  for i in range(1,len):
     if i< hlfwin:
        Sma[i]=(Sma[i-1]*i+S[i])/(i+1);
     if i==hlfwin:
        tmp= Sma[i-1]*i
        for j in range(hlfwin+1):
           tmp=tmp+  +S[i+j]
        Sma[i]=tmp/(2*hlfwin+1)
     if i>hlfwin and i <len-hlfwin:
       Sma[i]=Sma[i-1]+(S[i+hlfwin]-S[i-hlfwin-1])/(2*hlfwin+1)
     if i >=len-hlfwin:
       Sma[i]=(Sma[i-1]*(len-i+hlfwin+1)-S[i-1-hlfwin])/(len-i+hlfwin)
  return Sma
