import os, sys, csv,string
sys.path.append("/home/zeng/bin/lib/python2.6/site-packages/Numeric")
from Numeric import *
import subprocess
from RandomArray import *
from netCDF4 import Dataset
import MetrHas

if __name__ == '__main__':
    from optparse import OptionParser

    parser = OptionParser();

    parser.add_option("--end", dest="n",type="int", \
                    default="1000",  help = 'the total sample number ')
    parser.add_option("--start", dest="m" ,type="int", default="0", \
                    help='the start sample number')

    parser.add_option("--filenumber", dest="filenumber", default='0' ,type="int", \
                    help='file number')


    (options, args) = parser.parse_args()

    m=options.m
    n=options.n
    filenumber=options.filenumber

    proc=8
    files=[]
    jump=157
    for i in range(proc):
        file='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/pbsfile/task'+str(i+jump)
        files.append(file)


    VARIABLE_TO_CHANGE=['leafcn','fleafcn','fstemcn','organcn','frootcn','livewdcn']
    VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
    FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/OUTPUT_slope_GNMA/train_obs_slope_'+str(filenumber)
    TEXTFILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/obsdata'
    obsleaf= array([5.56, 10.69,13.81,47.31,67.03,93.56,111.54,156.76,195.64,210.74,199.91,208.45,164.25,96.88,0.00])*0.45
    obsstem= array([2.72,5.28,3.59,37.46,63.11,99.58,144.83,218.97,319.80,366.94,384.68,414.53,361.51,341.40,241.14])*0.45
    obsorgan= array([0.00,0.00,0.00,0.00,0.00,0.00,0.00,2.03,18.55,64.44,146.87,250.52,310.25,603.14,568.55])*0.45
    obslai=array([0.1,0.4,0.68,1.34,2.02,2.77,4.26,5.90,6.34,6.27,6.02,4.91,5.05,3.18,1.28])


    obs=array([obsleaf,obslai,obsorgan,obsstem])
    covmeanx='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/OUTPUT_slope_GNMA/covmeanx'
    obsdoy=array([146,153,160,167,174,181,188,195,202,209,216,223,230,244,251])
    obs_sl=array([obsleaf,obsstem])

    slope_obs=zeros((2,),Float)
    slope_obs[0]=(obsleaf[9]-obsleaf[2])/(obsdoy[9]-obsdoy[2]);
    slope_obs[1]=(obsstem[11]-obsstem[2])/(obsdoy[11]-obsdoy[2]);



    DOY=range(1,366)

    FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/USBo12004_L4_d.nc'
    VARIABLE=['GPP_or_MDS','NEE_or_fMDS']
    DOY=range(1,366)
    GNpft=array([[-1],[-1]])
    GNS= MetrHas.readdata(DOY,GNpft, VARIABLE,FILE)/86400.0
    obsgpp_0=GNS[0]
    obsnee_0=GNS[1]
    obsnee_neg_0=-1.0*obsnee_0

    S_gpp_pos=maximum(obsgpp_0,zeros((len(DOY),)))
    obsgpp=MetrHas.MovAve(S_gpp_pos,15)
    obsnee=MetrHas.MovAve(obsnee_0,15)
    obsnee_neg=-1.0*obsnee
    gpp_slope=(obsgpp[180]-obsgpp[160])/(20)
    nee_slope=(obsnee[180]-obsnee[160])/(20)


def txtread(TEXTFILE_TO_READ):
# z is an arrary proc X dim
   myfile=open(TEXTFILE_TO_READ, 'r')
   arr = []

   for line in myfile.readlines():
    # add a new sublist
      arr.append([])
    # loop over the elemets, split by whitespace
      for i in line.split():
         # convert to integer and append to the last
         # element of the list
          arr[-1].append(float(i))

   myfile.close()
   row= shape(arr)[0]
   col= shape(arr)[1]
   S=zeros((row,col),Float)
   for i in range(row):
      for j in range(col):
         S[i][j]=arr[i][j]
   return S

def wrapper(func, *args):
   return func(*args)




def  MaxLike_max(obs,S,Osigma):
      re_dim=obs.shape[0]
      #print S.shape,obs.shape
      diffOsum=0
      for j in range(0,re_dim):
             diffOsum=diffOsum+(max(obs[j])-max(S[j]))**2/Osigma[j]
             #print  max(obs[j])-max(S[j])

      return    diffOsum

def  MaxLike_slope(S,Osigma,outfile):
      re_dim=S.shape[0]
      #print re_dim
      #slope_obs=MetrHas.Slope2(obs,obsdoy)
      diffOsum=0
      slope=MetrHas.Slope1_1(S,outfile)

      for j in range(0,re_dim):
             diffOsum=diffOsum+(slope_obs[j]-slope[j])**2/Osigma[j]
             #print slope_obs[j],slope[j]

      return    diffOsum


def  MaxLike_gppnee(S,Osigma,rank,outfile):

      gpp=MetrHas.MovAve(S[0],15)
      nee=MetrHas.MovAve(S[1],15)


      nee_neg=-1.0*nee
      diffOsum=0
      a0=max(gpp)
      a1=max(nee_neg)


      diffOsum=diffOsum+(max(obsgpp)-a0)**2/Osigma[0]
      diffOsum=diffOsum+(max(obsnee_neg)-a1)**2/Osigma[1]

      iday,imonth= MetrHas.Slope_Month(0.3,outfile)
      gpp_slope_s=(gpp[iday+20]-gpp[iday])/20.0
      nee_slope_s=(nee[iday+20]-nee[iday])/20.0
      #print 'imonth: ', imonth,'neemonth: ', i1
      diffOsum=diffOsum+(gpp_slope- gpp_slope_s)**2/Osigma[2]
      diffOsum=diffOsum+(nee_slope- nee_slope_s)**2/Osigma[3]

      return    diffOsum

def  MaxLike(obs,S,Osigma,outfile,rank):
     Osigma1=Osigma[0:4]
     S1=array([S[0],S[1],S[2],S[3]])
     s1=MaxLike_max(obs,S1,Osigma1)
     S2=array([S[0],S[3]])
     Osigma2=array([Osigma[6],Osigma[7]])
     s2=MaxLike_slope(S2,Osigma2,outfile)
     Osigma3=array([Osigma[4],Osigma[5],Osigma[8],Osigma[9]])
     S3=array([S[4],S[5]])
     s3=MaxLike_gppnee(S3,Osigma3,rank,outfile)
     diffOsum=s1+s2+s3
     return diffOsum

def wrt_covmeanx(sigma,thetabar,theta,dim,filename):
   myfile=open(filename,'w')
   for t1 in range(dim):
       for t2 in range(dim):
                s=sigma[t1][t2]
                myfile.write ( "%.10e " % s)
       myfile.write ( "\n")
   for t1 in range(dim):
       s=thetabar[0][t1]
       myfile.write ( "%.10e " % s)
   myfile.write ( "\n")
   for t1 in range(proc):
       for t2 in range(dim):
          s=theta[t1][t2]
          myfile.write ( "%.10e " % s)
       myfile.write ( "\n")
   myfile.close()

def read_covmeanx(dim,filename):
   myfile=open(filename,'r')
   arr = []

   for line in myfile.readlines():
    # add a new sublist
      arr.append([])
    # loop over the elemets, split by whitespace
      for i in line.split():
         # convert to integer and append to the last
         # element of the list
          arr[-1].append(float(i))

   row= shape(arr)[0]
   col= shape(arr)[1]
   theta=zeros((proc,dim),Float)
   sigma=zeros((dim,dim),Float)
   thetabar=zeros((1,dim),Float)
   for i in range(dim):
     sigma[i]=arr[i]
   thetabar[0]=arr[dim]
   for i in range(proc):
     theta[i]=arr[dim+1+i]
   myfile.close()
   return sigma,thetabar,theta


def testlogpdf(z, obs,Osigma,Vsigma,Vmean):
    """
    test  pdf (Probability Density Function)
    """
    print "I am in testlogpdf\n"
    logpdf=zeros((proc,),Float)
    ch_dim=len(VARIABLE_TO_CHANGE)
    pft=ones((ch_dim,1))*20
    #dim=z.shape[1]
    #print z
    for i in range(proc):
       x=z[i]
       #print x
       #print x.shape
       FILE_TO_CHANGE='/fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(i+jump)+'.nc'
       MetrHas.changevar(x,pft,VARIABLE_TO_CHANGE,FILE_TO_CHANGE)
    print 'I am about to execute ccsm'
    ps = []
    for script in files:
      args = ["/bin/tcsh",  script]
      p = subprocess.Popen(args)
      ps.append(p)

    for p in ps:
      p.wait()

    print 'I am about to read the simulation results'

    dim=len(VARIABLE_TO_READ)
    re_dim=len(VARIABLE_TO_READ)
    pft=ones((re_dim,1))*20
    pft[re_dim-1]=4
    for i in range(proc):
         print 'I am reading results of task'+str(i+jump)
         FILE_TO_READ='/fusion/gpfs/home/zeng/exe/NEWPARA_'+str(i+jump)+'_US-Bo1_I_2000_CN/run/NEWPARA_'+str(i+jump)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000.nc'
         S= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

         diffOsum=0;
	 print "the difference of observation and simulation result"
         diffOsum=MaxLike(obs,S,Osigma,FILE_TO_READ,i+jump)
         diffVsum=dot((z[i]-Vmean) ,(z[i]-Vmean)/Vsigma)
         print "The total difference of observation and simulation result is ", diffOsum
         print  "The total difference of current sample and the control value is ", diffVsum
         logpdf[i]=(-(diffOsum+diffVsum)/2.0)
         os.system('rm '+FILE_TO_READ+'\n')
         os.system('rm /fusion/gpfs/home/zeng/exe/NEWPARA_'+str(i+jump)+'_US-Bo1_I_2000_CN/run/*.log.*'+'\n')
    return logpdf

#x is a proc X dim array
def MYMHfun(x,thetabar,sigma,testpdf, obs,Osigma,Vsigma,Vmean,n):
   dim=x.shape[1];
   #print dim
   vec = zeros((n+1,proc,dim),Float)
   can=zeros((proc,dim),Float)
   vec[0]=x
   counter=zeros((proc,),Int)
   acceptcounter=zeros((proc,),Int)
   theta=x

   oldlogpdf=wrapper(testpdf,x,obs,Osigma,Vsigma,Vmean)
   for keep in range(1,n+1):
      for  i in range(proc):
         y =multivariate_normal(theta[i],sigma,1)#candidate
         #print i,y.shape, "\n",exp(y)
         while  min(y)<=0:
            y =multivariate_normal(theta[i],sigma,1)
            counter[i]=counter[i]+1
         can[i]=reshape(y,(dim,))
      print "The candidate is ", can
      newlogpdf=wrapper(testpdf,can,obs,Osigma,Vsigma,Vmean)
      for i in range(proc):
        accept=MYMHfun_accept(newlogpdf[i], oldlogpdf[i])
        if accept==1:
          oldlogpdf[i]=newlogpdf[i]
          theta[i]=can[i]
          acceptcounter[i]=acceptcounter[i]+1
      for i in range(proc):
         index=(keep-1)*proc+i+1
         thetabar=MYMHfun_mean(index-1,thetabar,theta[i] )
         sigma=MYMHfun_var(index,thetabar,theta[i],sigma )
      vec[keep]=theta

      print  'Covariance SIGMA in the ' +str(keep)+'th iteration is ', sigma
      print 'Mean is in the ' +str(keep)+'th iteration is ', thetabar
   wrt_covmeanx(sigma,thetabar,theta,dim,covmeanx)
   print 'Covariance SIGMA is ', sigma
   print 'Mean is ', thetabar

   return vec, counter,acceptcounter


def MYMHfun_cont(x,thetabar,sigma,testpdf, obs,Osigma,Vsigma,Vmean,n,m):
   dim=x.shape[1];
   vec = zeros((n-m+1,proc,dim),Float)
   can=zeros((proc,dim),Float)
   vec[0]=x
   theta=x
   counter=zeros((proc,),Int)
   acceptcounter=zeros((proc,),Int)


   oldlogpdf=wrapper(testpdf,x,obs,Osigma,Vsigma,Vmean)
   for keep in range(1,n-m+1):
      for  i in range(proc):
         y =multivariate_normal(theta[i],sigma,1)#candidate
         while  min(y)<=0:
            y =multivariate_normal(theta[i],sigma,1)
            counter[i]=counter[i]+1
         can[i]=reshape(y,(dim,))
      print "The candidate is: ", y,"\n"
      newlogpdf=wrapper(testpdf,can,obs,Osigma,Vsigma,Vmean)
      for i in range(proc):
        accept=MYMHfun_accept(newlogpdf[i], oldlogpdf[i])
        if accept==1:
          oldlogpdf[i]=newlogpdf[i]
          theta[i]=can[i]
          acceptcounter[i]=acceptcounter[i]+1
      for i in range(proc):
         index=(keep-1+m)*proc+i+1
         thetabar=MYMHfun_mean(index-1,thetabar,theta[i] )
         sigma=MYMHfun_var(index,thetabar,theta[i],sigma )
      vec[keep]=theta

      print  'Covariance SIGMA in the ' +str(keep)+'th iteration is ', sigma,"\n"
      print 'Mean is in the ' +str(keep)+'th iteration is ', thetabar,"\n"
   wrt_covmeanx(sigma,thetabar,theta,dim,covmeanx)
   print 'The final Covariance SIGMA is ', sigma
   print 'The final Mean is ', thetabar

   return vec, counter,acceptcounter

def MYMHfun_mean(index,thetabar,theta ):
   thetabar=thetabar+(theta-thetabar)*(1.0/(index+1))
   #print 'the mean is:  ', thetabar
   return thetabar


def MYMHfun_var(index,thetabar,theta,sigma ):
   dif=theta-thetabar
   #print 'the difference is ', dif, 'the covariance is:',  sigma
   sigma=sigma*((index-1.0)/index)+(1.0/index)*matrixmultiply(transpose(dif), dif)
   #print 'the covariance then is:  ', sigma
   return sigma


def MYMHfun_accept(newlogpdf, oldlogpdf):

      #aprob = min([1.,exp(newlogpdf-oldlogpdf)]) #acceptance probability
      logaprob = min([0.,(newlogpdf-oldlogpdf)])
      u = uniform(0,1)
      logu=log(u)
      print 'the logrithm randon uniform number is ', logu, 'the ratio is ',logaprob
      #if u < aprob:
      if logu<logaprob:
          accept = 1
      else:
          accept = 0
      return accept

def wrtfile(i,S):

   FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/ncdata'+str(i)
   re_dim= shape(S)[0]
   num= shape(S)[1]
   myfile = open(FILE_TO_WRITE, 'w')
   for j in range(num):
       for i in range(re_dim):
                s=S[i][j]
                myfile.write ( "%.10e " % s)
       myfile.write ( "\n")
   myfile.close()


#Osigma=array([0.01,0.5,0.5,1.0e-2,1.0e-2],Float)
#Osigma=array([0.025,46,22,0.25,0.0034],Float)
#Osigma=array([4,0.08,32,39,2.50e-11,1.20e-12, 4.0e-3,  9.0e-3,  1.1e-012, 7.e-013],Float)
#Osigma=array([4,0.08,32,39,9.0e-12,3.0e-12, 2.0e-3,  4.0e-3,  2.0e-012, 7.0e-013],Float)
#Osigma=array([4.1,0.007,32,39,2.30e-11,1.0e-11, 2.0e-3,  4.0e-3,  2.0e-012, 7.0e-013],Float)
#Osigma=array([4.1,0.007,32,39,2.9e-011, 1.4e-011,  2.0e-3, 4.0e-3,  4.0e-015 ,    2.0e-015],Float)
if __name__ == '__main__':
    Osigma=array([5.7, 0.03, 46.1,22,2.4e-011, 5.1e-012, 0.0021 ,0.0054, 1.2e-014,  3.8e-015],Float)
    z=(array([25.00,65.00,130.00,60.00,42.00,50.00],Float))
    dim=len(z)
    x=zeros((proc,dim),Float)

    if m==0:
        x[0]=array([17.5, 45.5, 91, 42, 29.4, 35],Float)
        x[1]=array([32.5, 84.5, 169, 78, 54.6, 65],Float)
        x[2]=array([25, 84.5, 130, 78, 42, 65],Float)
        x[3]=array([25, 45.5, 130, 42, 42, 35],Float)
        x[4]=array([17.5, 65, 91, 60, 29.4, 50],Float)
        x[5]=array([32.5, 65, 169, 60, 54.6, 50],Float)
        x[6]=array([17.5, 84.5, 91, 78, 29.4, 65],Float)
        x[7]=array([32.5, 45.5, 169, 42, 54.6, 35],Float)


        alpha=1e-1
        thetabar=zeros((1,dim),Float)
        sigma= identity(dim,Float)*alpha
    else:
        sigma,thetabar,x=read_covmeanx(dim,covmeanx)
        os.system('mv '+ covmeanx + '  '+covmeanx+ str(filenumber) )



    #Vsigma=array([1,1,1,1,1,1],Float)
    #Vsigma=array([4,25,100,22.5,11,16],Float)
    Vsigma=array([3.90625, 26.4063, 105.625, 22.5, 11.025, 15.625 ],Float)
    Vmean=z
    smpl,counter,acceptcounter=MYMHfun_cont(x,thetabar,sigma,testlogpdf,obs,Osigma,Vsigma,Vmean,n,m)
    #smpl,counter,acceptcounter=MYMHfun(x,thetabar,sigma,testlogpdf,obs,Osigma,Vsigma,Vmean,n)
    #plotting the results:
    trans=zip(*smpl)
    print trans
    print "negative counter is ",counter
    print "accept counter is ",acceptcounter


    if m==0:
        myfile = open(FILE_TO_WRITE, 'w')

    for i in range(proc):
        for j in range(n-m+1):
            for k in range(dim):
                s=smpl[j][i][k]
                myfile.write ( "%.10e " % s)
            myfile.write ( "\n")
        myfile.close()

    else:
     myfile = open(FILE_TO_WRITE, 'w')

    for i in range(proc):
        for j in range(1,n-m+1):
            for k in range(dim):
                s=smpl[j][i][k]
                myfile.write ( "%.10e " % s)
            myfile.write ( "\n")
        myfile.close()
