import os, sys, csv,string
sys.path.append("/home/zeng/bin/lib/python2.6/site-packages/Numeric")
from Numeric import *
import subprocess
from RandomArray import *
from netCDF4 import Dataset
import MetrHas
HF=15
def MovAve(S,hlfwin):
  len=S.shape[0]
  il=len-1
  Sma=zeros((len,),Float)
  Sma[0]=S[0]
  for i in range(1,len):
     if i< hlfwin:
        Sma[i]=(Sma[i-1]*i+S[i])/(i+1);
     if i==hlfwin:
        tmp= Sma[i-1]*i
        for j in range(hlfwin+1):
           tmp=tmp+  +S[i+j]
        Sma[i]=tmp/(2*hlfwin+1)  
     if i>hlfwin and i <len-hlfwin:
       Sma[i]=Sma[i-1]+(S[i+hlfwin]-S[i-hlfwin-1])/(2*hlfwin+1)
     if i >=len-hlfwin:
       Sma[i]=(Sma[i-1]*(len-i+hlfwin+1)-S[i-1-hlfwin])/(len-i+hlfwin)
  return Sma





FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/USBo12002_L4_d.nc' 
VARIABLE=['GPP_or_MDS','NEE_or_fMDS']  
DOY=range(1,366)
pft=array([[-1],[-1]])
GNS= MetrHas.readdata(DOY,pft, VARIABLE,FILE)/86400
obsgpp_0=GNS[0]
obsnee_0=GNS[1]
obsnee_neg_0=-1.0*obsnee_0

S_gpp_pos=maximum(obsgpp_0,zeros((len(DOY),)))
obsgpp=MetrHas.MovAve(S_gpp_pos,15)
obsnee=MetrHas.MovAve(obsnee_0,15)
"""
obsleaf= array([5.56, 10.69,13.81,47.31,67.03,93.56,111.54,156.76,195.64,210.74,199.91,208.45,164.25,96.88,0.00])*0.45
obsstem= array([2.72,5.28,3.59,37.46,63.11,99.58,144.83,218.97,319.80,366.94,384.68,414.53,361.51,341.40,241.14])*0.45
obsorgan= array([0.00,0.00,0.00,0.00,0.00,0.00,0.00,2.03,18.55,64.44,146.87,250.52,310.25,603.14,568.55])*0.45
obslai=array([0.1,0.4,0.68,1.34,2.02,2.77,4.26,5.90,6.34,6.27,6.02,4.91,5.05,3.18,1.28])
 
obsdoy=array([146,153,160,167,174,181,188,195,202,209,216,223,230,244,251])
"""
obsleaf= array([10.1,20.3,56.0,67.4,105.9,133.9,147.4,226.2,216.1,174.5,182.9,222.2,85.3,0.0])*0.45
obsstem= array([7.4,13.1,38.1,64.3,110.7,163.6,196.9,254.6,287.4,228.8,251.5,425.2,168.4,132.1])*0.45
obsorgan= array([0.0,0.0,0.0,0.0,0.0,0.0,7.4,49.9,113.7,161.9,318.9,385.4,349.1,461.9])*0.45
obslai=array([0.11,0.61,1.31,1.65,2.20,3.02,4.78,4.14,5.62,5.15,4.07,4.14,3.29,0.00])
obsdoy=array([178,183,190,197,204,211,218,225,232,239,247,253,260,267])

myrank=158

#####control part###########
####pft,initial default########

os.system('cp /homes/zeng/exe/PARA_10_US-Bo1_I_2000_CN/run/PARA_10_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop_orig.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')

#####read control value for inital value and pft #######

VARIABLE_TO_READ=['leafcn','fleafcn','fstemcn','organcn','frootcn','livewdcn']
pftfile='/fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc'
d=len(VARIABLE_TO_READ)
pft=-1.0*ones((d,1))
dd=[21]
control= MetrHas.readdata(dd,pft, VARIABLE_TO_READ,pftfile)
print 'pft control:', control


####ccsm######

file='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/pbsfile/task'+str(myrank)
os.system(file)
os.system('cp /fusion/gpfs/home/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000.nc /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-control-715.nc')
####read output #########
DOY=range(1,366)
VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']

FILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-control-715.nc'
pft=ones((6,1))*20
pft[5]=4
Scon= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

gpp_pos=maximum(Scon[4],zeros(len(DOY,)))
gpp_con=MetrHas.MovAve(gpp_pos,15)
nee_con=MetrHas.MovAve(Scon[5],15)


#####validation part###########
myrank=105
###pft and initial back to default ###
os.system('cp /homes/zeng/exe/PARA_10_US-Bo1_I_2000_CN/run/PARA_10_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop_orig.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')


#####change pft ########
PFT_VARIABLE_TO_CHANGE=['leafcn','fleafcn','fstemcn','organcn','frootcn','livewdcn']


FILE_TO_CHANGE='/fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc'

#z=array([13.2721226692333,  60.7931149835255,  114.871745362293,  47.0919613501083,  49.7670237169443 ,   67.0775744519408],Float)
z=array([13.273827596,  60.771808046 ,   114.98821273, 46.797919126,  49.723627565,   67.088863528],Float)


pft=ones((6,1))*20
MetrHas.changevar(z,pft,PFT_VARIABLE_TO_CHANGE,FILE_TO_CHANGE)

###ccsm#####
file='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/pbsfile/task'+str(myrank)
os.system(file)
os.system('cp /fusion/gpfs/home/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000.nc /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-validation-715.nc')

#####read output#######

VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
DOY=range(1,366)
pft=ones((6,1))*20
pft[5]=4
outfile='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-validation-715.nc'

Scal= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,outfile)
gpp_pos=maximum(Scal[4],zeros(len(DOY,)))
gpp_cal=MetrHas.MovAve(gpp_pos,15)
nee_cal=MetrHas.MovAve(Scal[5],15)


###pft and initial back to default ###
os.system('cp /homes/zeng/exe/PARA_10_US-Bo1_I_2000_CN/run/PARA_10_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0715-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop_orig.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')

#####plot######
from matplotlib.pylab import *

fig=figure()

subplot(321)

plot(DOY,Scal[0],'ro')
plot(DOY,Scon[0],'y>')
plot(obsdoy,obsleaf,'b*')
#legend(('calibration','control','observation')) 
ylabel('LEAFC')
xlabel('DOY')
subplot(322)

plot(DOY,Scal[1],'ro')
plot(DOY,Scon[1],'y>')
plot(obsdoy,obslai,'b*')
#legend(('calibration','control','observation')) 
ylabel('TLAI')
xlabel('DOY')

subplot(323)

plot(DOY,Scal[2],'ro')
plot(DOY,Scon[2],'y>')
plot(obsdoy,obsorgan,'b*')
#legend(('calibration','control','observation')) 
ylabel('ORGANC')
xlabel('DOY')

subplot(324)

plot(DOY,Scal[3],'ro')
plot(DOY,Scon[3],'y>')
plot(obsdoy,obsstem,'b*')
#legend(('calibration','control','observation')) 
ylabel('STEMC')
xlabel('DOY')


subplot(325)
 
plot(DOY,gpp_cal,'ro')
plot(DOY,gpp_con,'y>')
plot(DOY,obsgpp,'b*')
#legend(('calibration','control','observation'))  
ylabel('GPP')
xlabel('DOY')
subplot(326)
 
plot(DOY,nee_cal,'ro')
plot(DOY,nee_con,'y>')
plot(DOY,obsnee,'b*')
#legend(('calibration','control','observation')) 
ylabel('NEE')
xlabel('DOY')

"""
plot(DOY,gpp_cal,'ro')
plot(DOY,gpp_con,'y>')
plot(DOY,obsgpp,'b*')
legend(('calibration','control','observation'))  
ylabel('GPP')
xlabel('DOY')
#legend('calibration','control','pbservation')


plot(DOY,Scal[2],'ro')
plot(DOY,Scon[2],'y>')
plot(obsdoy,obsorgan,'b*-')
legend(('calibration','control','observation')) 
ylabel('ORGANC')
xlabel('DOY')



plot(DOY,Scal[3],'ro')
plot(DOY,Scon[3],'y>')
plot(obsdoy,obsstem,'b*-')
legend(('calibration','control','observation')) 
ylabel('STEMC')
xlabel('DOY')
"""
show()
os.system('cd /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4_tbase')

from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('multipage_valid.pdf')

fig.savefig(pp, format='pdf')
pp.close()  
