import os, sys, csv,string
sys.path.append("/home/zeng/bin/lib/python2.6/site-packages/Numeric")
from Numeric import *
import subprocess
from RandomArray import *
from netCDF4 import Dataset
import MetrHas
HF=15
def MovAve(S,hlfwin):
  len=S.shape[0]
  il=len-1
  Sma=zeros((len,),Float)
  Sma[0]=S[0]
  for i in range(1,len):
     if i< hlfwin:
        Sma[i]=(Sma[i-1]*i+S[i])/(i+1);
     if i==hlfwin:
        tmp= Sma[i-1]*i
        for j in range(hlfwin+1):
           tmp=tmp+  +S[i+j]
        Sma[i]=tmp/(2*hlfwin+1)  
     if i>hlfwin and i <len-hlfwin:
       Sma[i]=Sma[i-1]+(S[i+hlfwin]-S[i-hlfwin-1])/(2*hlfwin+1)
     if i >=len-hlfwin:
       Sma[i]=(Sma[i-1]*(len-i+hlfwin+1)-S[i-1-hlfwin])/(len-i+hlfwin)
  return Sma





FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/trainobs.nc' 

myrank=9

#####control part###########
####pft,initial default########

os.system('cp /homes/zeng/exe/PARA_1_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc')
#os.system('cp /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop30.c110307.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')

#####read control value for inital value and pft #######

VARIABLE_TO_READ=['leafcn','fleafcn','fstemcn','organcn','frootcn','livewdcn']
pftfile='/fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc'
d=len(VARIABLE_TO_READ)
pft=-1.0*ones((d,1))
dd=[21]
control= MetrHas.readdata(dd,pft, VARIABLE_TO_READ,pftfile)
print 'pft control:', control


####ccsm######

file='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/pbsfile/task'+str(myrank)
#os.system(file)
#os.system('cp /fusion/gpfs/home/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000.nc /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/')
####read output #########
DOY=range(1,366)
VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']

FILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000.nc'
pft=ones((6,1))*20
pft[5]=4
Scon= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

gpp_pos=maximum(Scon[4],zeros(len(DOY,)))
gpp_con=MetrHas.MovAve(gpp_pos,15)
nee_con=MetrHas.MovAve(Scon[5],15)


#####Generate  part###########
myrank=10
###pft and initial back to default ###
os.system('cp /homes/zeng/exe/PARA_1_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop30.c110307.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')


#####change pft ########
PFT_VARIABLE_TO_CHANGE=['leafcn','fleafcn','fstemcn','organcn','frootcn','livewdcn']


FILE_TO_CHANGE='/fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc'

z=array([],Float)
pft=ones((6,1))*20
MetrHas.changevar(z,pft,PFT_VARIABLE_TO_CHANGE,FILE_TO_CHANGE)

###ccsm#####
file='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/pbsfile/task'+str(myrank)
os.system(file)
os.system('cp /fusion/gpfs/home/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000.nc ' +FILE)

#####read output#######

VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
DOY=range(1,366)
pft=ones((6,1))*20
pft[5]=4
outfile='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000.nc'

Scal= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,outfile)
gpp_pos=maximum(Scal[4],zeros(len(DOY,)))
gpp_cal=MetrHas.MovAve(gpp_pos,15)
nee_cal=MetrHas.MovAve(Scal[5],15)


###pft and initial back to default ###
os.system('cp /homes/zeng/exe/PARA_1_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc  /homes/zeng/exe/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN/run/PARA_1_US-Bo1_I_2000_CN.clm2.r.0717-01-01-00000.nc')
os.system('cp /pvfs/bbye/CESM_input/my_inputdata/lnd/clm2/pftdata/pft-physiology_crop30.c110307.nc    /fusion/gpfs/home/zeng/inputdata/lnd/clm2/pftdata/bruteforce_all/pft-physiology_crop30.c110307_'+str(myrank)+'.nc')

#####plot######
from matplotlib.pylab import *

fig=figure()

subplot(321)

plot(DOY,Scal[0],'ro')
plot(DOY,Scon[0],'y>')

#legend(('calibration','control','observation')) 
ylabel('LEAFC')
xlabel('DOY')
subplot(322)

plot(DOY,Scal[1],'ro')
plot(DOY,Scon[1],'y>')

#legend(('calibration','control','observation')) 
ylabel('TLAI')
xlabel('DOY')

subplot(323)

plot(DOY,Scal[2],'ro')
plot(DOY,Scon[2],'y>')

#legend(('calibration','control','observation')) 
ylabel('ORGANC')
xlabel('DOY')

subplot(324)

plot(DOY,Scal[3],'ro')
plot(DOY,Scon[3],'y>')

#legend(('calibration','control','observation')) 
ylabel('STEMC')
xlabel('DOY')


subplot(325)
 
plot(DOY,gpp_cal,'ro')
plot(DOY,gpp_con,'y>')

#legend(('calibration','control','observation'))  
ylabel('GPP')
xlabel('DOY')
subplot(326)
 
plot(DOY,nee_cal,'ro')
plot(DOY,nee_con,'y>')

#legend(('calibration','control','observation')) 
ylabel('NEE')
xlabel('DOY')
show()

os.system('cd /fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4')

from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('multipage.pdf')

fig.savefig(pp, format='pdf')
pp.close()  
