import os, sys, csv,string
sys.path.append("/home/zeng/bin/lib/python2.6/site-packages/Numeric")
from Numeric import *
import subprocess
from RandomArray import *
from netCDF4 import Dataset
import MetrHas
HF=15
def MovAve(S,hlfwin):
  len=S.shape[0]
  il=len-1
  Sma=zeros((len,),Float)
  Sma[0]=S[0]
  for i in range(1,len):
     if i< hlfwin:
        Sma[i]=(Sma[i-1]*i+S[i])/(i+1);
     if i==hlfwin:
        tmp= Sma[i-1]*i
        for j in range(hlfwin+1):
           tmp=tmp+  +S[i+j]
        Sma[i]=tmp/(2*hlfwin+1)  
     if i>hlfwin and i <len-hlfwin:
       Sma[i]=Sma[i-1]+(S[i+hlfwin]-S[i-hlfwin-1])/(2*hlfwin+1)
     if i >=len-hlfwin:
       Sma[i]=(Sma[i-1]*(len-i+hlfwin+1)-S[i-1-hlfwin])/(len-i+hlfwin)
  return Sma





FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/USBo12004_L4_d.nc' 
VARIABLE=['GPP_or_MDS','NEE_or_fMDS']  
DOY=range(1,366)
pft=array([[-1],[-1]])
GNS= MetrHas.readdata(DOY,pft, VARIABLE,FILE)/86400
obsgpp_0=GNS[0]
obsnee_0=GNS[1]
obsnee_neg_0=-1.0*obsnee_0

S_gpp_pos=maximum(obsgpp_0,zeros((len(DOY),)))
obsgpp=MetrHas.MovAve(S_gpp_pos,15)
obsnee=MetrHas.MovAve(obsnee_0,15)

obsleaf= array([5.56, 10.69,13.81,47.31,67.03,93.56,111.54,156.76,195.64,210.74,199.91,208.45,164.25,96.88,0.00])*0.45
obsstem= array([2.72,5.28,3.59,37.46,63.11,99.58,144.83,218.97,319.80,366.94,384.68,414.53,361.51,341.40,241.14])*0.45
obsorgan= array([0.00,0.00,0.00,0.00,0.00,0.00,0.00,2.03,18.55,64.44,146.87,250.52,310.25,603.14,568.55])*0.45
obslai=array([0.1,0.4,0.68,1.34,2.02,2.77,4.26,5.90,6.34,6.27,6.02,4.91,5.05,3.18,1.28])
 
obsdoy=array([146,153,160,167,174,181,188,195,202,209,216,223,230,244,251])

myrank=106

#####717-control part###########
####read output #########
DOY=range(1,366)
VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']

FILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000-control.nc'
pft=ones((6,1))*20
pft[5]=4
Scon= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

gpp_pos=maximum(Scon[4],zeros(len(DOY,)))
gpp_con=MetrHas.MovAve(gpp_pos,15)
nee_con=MetrHas.MovAve(Scon[5],15)
Scon[4]=gpp_con
Scon[5]=nee_con
print Scon.shape
FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/control717' 
myfile = open(FILE_TO_WRITE, 'w') 
for i in range(6):
      for j in range(365):
        #print i,j,'\n' 
        s=Scon[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()

#####717-calibration part###########
#####read output#######

VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
DOY=range(1,366)
pft=ones((6,1))*20
pft[5]=4
outfile='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0717-01-01-00000-calibration.nc'

Scal= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,outfile)
gpp_pos=maximum(Scal[4],zeros(len(DOY,)))
gpp_cal=MetrHas.MovAve(gpp_pos,15)
nee_cal=MetrHas.MovAve(Scal[5],15)
Scal[4]=gpp_cal
Scal[5]=nee_cal



FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/calibration717'
myfile = open(FILE_TO_WRITE, 'w')
for i in range(6):
      for j in range(365):
        s=Scal[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()

myrank=105

######observations 2004##########

FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/USBo12004_L4_d.nc'
VARIABLE=['GPP_or_MDS','NEE_or_fMDS']
DOY=range(1,366)
pft=array([[-1],[-1]])
GNS= MetrHas.readdata(DOY,pft, VARIABLE,FILE)/86400
obsgpp_0=GNS[0]
obsnee_0=GNS[1]
obsnee_neg_0=-1.0*obsnee_0

S_gpp_pos=maximum(obsgpp_0,zeros((len(DOY),)))
obsgpp=MetrHas.MovAve(S_gpp_pos,15)
obsnee=MetrHas.MovAve(obsnee_0,15)
Sobs=zeros((2,365),Float)
Sobs[0]=obsgpp 
Sobs[1]=obsnee
FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/observation717'    
myfile = open(FILE_TO_WRITE, 'w')
for i in range(2):
      print i
      for j in range(365):
      
        s=Sobs[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()

######observations 2002##########
 
FILE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/USBo12002_L4_d.nc'
VARIABLE=['GPP_or_MDS','NEE_or_fMDS']
DOY=range(1,366) 
pft=array([[-1],[-1]]) 
GNS= MetrHas.readdata(DOY,pft, VARIABLE,FILE)/86400
obsgpp_0=GNS[0] 
obsnee_0=GNS[1] 
obsnee_neg_0=-1.0*obsnee_0 
Sobs=zeros((2,365),Float)
 
S_gpp_pos=maximum(obsgpp_0,zeros((len(DOY),)))
obsgpp=MetrHas.MovAve(S_gpp_pos,15)
obsnee=MetrHas.MovAve(obsnee_0,15)
Sobs[0]=obsgpp   
Sobs[1]=obsnee
FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/observation715'
myfile = open(FILE_TO_WRITE, 'w')
for i in range(2):
      for j in range(365):
        s=Sobs[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()

#####715-control part###########
####read output #########
DOY=range(1,366)
VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
 
FILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-control-715.nc'
pft=ones((6,1))*20
pft[5]=4 
Scon= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

gpp_pos=maximum(Scon[4],zeros(len(DOY,)))
gpp_con=MetrHas.MovAve(gpp_pos,15)
nee_con=MetrHas.MovAve(Scon[5],15)
 
Scon[4]=gpp_con 
Scon[5]=nee_con
FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/control715'    
myfile = open(FILE_TO_WRITE, 'w')
for i in range(6): 
      for j in range(365):
        s=Scon[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close() 
 
#####715-validattion part###########
#####read output#######

VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
DOY=range(1,366)
pft=ones((6,1))*20
pft[5]=4
outfile='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-validation-715.nc'

Scal= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,outfile)
gpp_pos=maximum(Scal[4],zeros(len(DOY,)))
gpp_cal=MetrHas.MovAve(gpp_pos,15)
nee_cal=MetrHas.MovAve(Scal[5],15)
Scal[4]=gpp_cal
Scal[5]=nee_cal



FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/validation715'
myfile = open(FILE_TO_WRITE, 'w')
for i in range(6):
      for j in  range(365):
        s=Scal[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()


#####717-control part-717###########
####read output #########
DOY=range(1,366)
VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
 
FILE_TO_READ='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-control-717.nc'
pft=ones((6,1))*20
pft[5]=4 
Scon= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,FILE_TO_READ)

gpp_pos=maximum(Scon[4],zeros(len(DOY,)))
gpp_con=MetrHas.MovAve(gpp_pos,15)
nee_con=MetrHas.MovAve(Scon[5],15)
Scon[4]=gpp_con 
Scon[5]=nee_con
FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/control715_717'    
myfile = open(FILE_TO_WRITE, 'w')
for i in range(6): 
      for j in  range(365):
        s=Scon[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close() 
 
 
#####717-validation par-717t###########
#####read output#######

VARIABLE_TO_READ=['LEAFC','TLAI','ORGANC','STEMC','GPP','NEE']
DOY=range(1,366)
pft=ones((6,1))*20
pft[5]=4
outfile='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/NEWPARA_'+str(myrank)+'_US-Bo1_I_2000_CN.clm2.h1.0715-01-01-00000-validation-717.nc'

Scal= MetrHas.readdata(DOY,pft, VARIABLE_TO_READ,outfile)
gpp_pos=maximum(Scal[4],zeros(len(DOY,)))
gpp_cal=MetrHas.MovAve(gpp_pos,15)
nee_cal=MetrHas.MovAve(Scal[5],15)
Scal[4]=gpp_cal
Scal[5]=nee_cal



FILE_TO_WRITE='/fusion/gpfs/home/zeng/bbye_clm4/scripts/PTCLM_files/TRAIN/train4/validation715_717'
myfile = open(FILE_TO_WRITE, 'w')
for i in range(6):
      for j in  range(365):
        s=Scal[i][j]
        myfile.write ( "%.10e " % s)
      myfile.write ( "\n")
myfile.close()

