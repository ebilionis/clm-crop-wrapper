"""
Compare if the moving average we compute is the same as Zeng
and figure out exactly what she computes.
"""

import numpy as np
import cPickle as pickle
import matplotlib.pyplot as plt
import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import clm
from netCDF4 import Dataset


# This is Zengs code:
from zengs_code.MetrHas import Slope_Month as zengs_slope_month
from zengs_code.MetrHas import Slope1_1 as zengs_slope


if __name__ == '__main__':
    # The history file to look at
    output_file = os.path.abspath('test_2004_site=bondvilleIL_res=1pt_1pt_compset=I_2000_CN.clm2.h1.0001-01-01-00000.nc')
    with Dataset(output_file, 'r') as fd:
        GDDFRAC = fd.variables['GDDFRAC'][:, 20] # The 20 is the pft index for soy
    observed_data_file = os.path.abspath('../data/USBo12004_observed.pickle')
    with open(observed_data_file, 'rb') as fd:
        observed_data = pickle.load(fd)
    # Load the data
    gdd_frac = 0.3
    day, month = zengs_slope_month(gdd_frac, output_file)
    my_day = clm.find_index_data_exceeds_value(GDDFRAC, gdd_frac)
    my_month = clm.find_month_of_day(my_day)
    print 'Zeng: day=', day, 'month=', month
    print 'Me: day=', day, 'month=', month

    for var_name in ['LEAFC', 'TLAI', 'ORGANC', 'STEMC', 'GPP', 'NEE']:
        print 'Testing output:', var_name
        pft_index = 4 if var_name == 'NEE' else 20
        with Dataset(output_file, 'r') as fd:
            data = fd.variables[var_name][:, pft_index]
        obs_data = observed_data[var_name]['data']
        obs_doy = observed_data[var_name]['doy']
        if var_name == 'GPP' or var_name == 'NEE':
            data = clm.get_moving_average(data, window=30)
            obs_data = clm.get_moving_average(obs_data, window=30)
        if var_name == 'NEE':
            max_data_idx = np.argmin(data)
            max_obs_data_idx = np.argmin(obs_data)
        else:
            max_data_idx = np.argmax(data)
            max_obs_data_idx = np.argmax(obs_data)
        max_data = data[max_data_idx]
        max_obs_data_doy = obs_doy[max_obs_data_idx]
        max_obs_data = obs_data[max_obs_data_idx]
        # PLOT THE DATA AND THEIR MAXIMUM
        # OBSERVED DATA
        plt.plot(obs_doy, obs_data, '*k', markersize=10)
        plt.plot(np.arange(max_obs_data_doy), [max_obs_data] * max_obs_data_doy,
                 ':k', linewidth=2)
        # SAMPLE CLM-CROP DATA
        plt.plot(data, 'b', linewidth=2)
        plt.plot(np.arange(max_data_idx), [max_data] * max_data_idx,
                 ':b', linewidth=2)
        y_lower = min(min(obs_data), min(data))
        y_upper = 2 * max(max(obs_data), max(data))
        legend = ['Observed', 'Max of Observed', 'Sample CLM-Crop', 'Max of CLM-Crop']
        if not (var_name == 'TLAI' or var_name == 'ORGANC'):
            if var_name == 'NEE' or var_name == 'GPP':
                z_obs_slope = (obs_data[180] - obs_data[160]) / 20.
                z_obs_day_idx = 160
                z_obs_day = z_obs_day_idx
            elif var_name == 'LEAFC':
                z_obs_slope = (obs_data[9] - obs_data[2]) / (obs_doy[9] - obs_doy[2])
                z_obs_day_idx = 2
                z_obs_day = obs_doy[z_obs_day_idx]
            else: # var_name == 'STEMC'
                z_obs_slope = (obs_data[11] - obs_data[2]) / (obs_doy[11] - obs_doy[2])
                z_obs_day_idx = 2
                z_obs_day = obs_doy[z_obs_day_idx]
            z_slope = zengs_slope(np.atleast_2d(data), output_file)[0]
            my_slope, my_beta = clm.get_slope_at_index(data, day, 30)
            print 'Zeng: obs slope=', z_obs_slope
            print 'Zeng: slope=', z_slope
            print 'Me: slope=', my_slope
            obs_days_around_day = np.arange(max(z_obs_day - 30, 0),
                                            min(z_obs_day + 30, 365))
            plt.plot(obs_days_around_day,
                     (obs_days_around_day - z_obs_day) * z_obs_slope + obs_data[z_obs_day_idx],
                     '.k', linewidth=2)
            days_around_day = np.arange(max(day - 30, 0), min(day + 30, 365))
            plt.plot(days_around_day,
                     (days_around_day - day) * z_slope + data[day],
                     '.g', linewidth=2)
            plt.plot(days_around_day, days_around_day * my_slope + my_beta,
                     '.m', linewidth=2)
            legend += ['Observed Slope (Zeng)', 'Slope of CLM-Crop (Zeng)',
                       'Slope of CLM-Crop (Ilias)']
            plt.plot(np.arange(day), [data[day]] * day, '--r', linewidth=2)
            plt.plot([day] * 20, np.linspace(0, data[day], 20), '--r', linewidth=2)
        plt.xlabel('DoY', fontsize=16)
        plt.ylabel(var_name, fontsize=16)
        plt.legend(legend, loc='best')
        plt.ylim([y_lower, y_upper])
        plt.grid(True)
        plt.show()
