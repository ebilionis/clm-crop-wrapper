.. CLM-Crop Python Wrapper documentation master file, created by
   sphinx-quickstart on Mon Oct 14 16:41:58 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CLM-Crop Python Wrapper's documentation!
===================================================

Contents:

.. toctree::
   :maxdepth: 2

   tutorial
   reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

