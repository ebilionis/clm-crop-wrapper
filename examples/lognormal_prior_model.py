"""
A first naive prior model for the parameters of interest.
"""


import pymc as pm
import math


free_params=[{'name': 'leafcn', 'idx': 23,
              'pdf': pm.Lognormal('leafcn', mu=math.log(25.), tau=9.)},
             {'name': 'fleafcn', 'idx': 23,
              'pdf': pm.Lognormal('fleafcn', mu=math.log(65.), tau=9.)},
             {'name': 'livewdcn', 'idx': 23,
              'pdf': pm.Lognormal('livewdcn', mu=math.log(50.), tau=9.)},
             {'name': 'fstemcn', 'idx': 23,
              'pdf': pm.Lognormal('fstemcn', mu=math.log(130.), tau=9.)},
             {'name': 'frootcn', 'idx': 23,
              'pdf': pm.Lognormal('frootcn', mu=math.log(42.), tau=9.)},
             {'name': 'graincn', 'idx': 23,
              'pdf': pm.Lognormal('graincn', mu=math.log(60.), tau=9.)},
             {'name': 'slatop', 'idx': 23,
              'pdf': pm.Uniform('slatop', 0.05, 0.09)}]
