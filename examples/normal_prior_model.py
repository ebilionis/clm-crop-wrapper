"""
A second naive prior model for the parameters of interest.
"""


import pymc as pm
import math


free_params=[{'name': 'leafcn', 'idx': 23,
              'pdf': pm.Normal('leafcn', 25., tau=1. / 3.90625)},
             {'name': 'fleafcn', 'idx': 23,
              'pdf': pm.Normal('fleafcn', 65., tau=1. / 26.4063)},
             {'name': 'livewdcn', 'idx': 23,
              'pdf': pm.Normal('livewdcn', mu=50., tau=1. / 15.625)},
             {'name': 'fstemcn', 'idx': 23,
              'pdf': pm.Normal('fstemcn', mu=130., tau=1. / 105.625)},
             {'name': 'frootcn', 'idx': 23,
              'pdf': pm.Normal('frootcn', mu=42., tau=1. / 11.025)},
             {'name': 'graincn', 'idx': 23,
              'pdf': pm.Normal('graincn', mu=60., tau=1. / 22.5)},
             {'name': 'slatop', 'idx': 23,
              'pdf': pm.Normal('slatop', mu=0.07, tau=1. / 0.01)}]
