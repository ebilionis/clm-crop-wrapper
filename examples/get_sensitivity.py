import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import clm
import _clm_surrogate
import pysmc as ps


if __name__ == '__main__':
    output_names = ['LEAFC', 'TLAI', 'ORGANC', 'STEMC', 'GPP', 'NEE']
    with open('../data/clm_crop_surrogate_logprior_rvm.pickle', 'rb') as fd:
        surrogate = pickle.load(fd)
    for out in output_names:
        f = surrogate._comp_surrogates[out]['f']
        # The product basis
        pb = f.basis.screened_func
        # The indices of the polynomials that are retained
        idx = f.basis.out_idx
        # All the terms of the basis
        terms = pb.terms
        print out
        for i in idx:
            print terms[i]
