import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import clm
import _clm_surrogate
import pysmc as ps


if __name__ == '__main__':
    output_names = ['LEAFC', 'TLAI', 'ORGANC', 'STEMC', 'GPP', 'NEE']
    y_con = np.loadtxt('control_out.dat')
    db = ps.DataBase.load('new_sur_db.pickle')
    with open('../data/SurFakeOutputs.pickle', 'rb') as fd:
        y_obs = pickle.load(fd)
    with open('../data/clm_crop_surrogate_logprior_rvm.pickle', 'rb') as fd:
        surrogate = pickle.load(fd)
    count = 0
    for out in output_names:
        idx = np.arange(count, count + 365)
        d_con = y_con[idx]
        d_obs = y_obs[out]['data']
        if out == 'GPP' or out == 'NEE':
            d_con = clm.get_moving_average(d_con, 30)
            d_obs = clm.get_moving_average(d_obs, 30)
        plt.plot(d_con, 'b', linewidth=2)
        for i in range(1):
            d_cal = surrogate.evaluate_original(db.particle_approximation.input[0, :])[idx]
            plt.plot(d_cal, 'r', linewidth=2)
        plt.plot(y_obs[out]['doy'], d_obs, 'ko', markersize=5)
        plt.xlabel('doy', fontsize=16)
        plt.ylabel(out, fontsize=16)
        plt.legend(['Control', '\'Calibrated\'', 'Observed'], loc='best')
        png_file = 'self_reduced_' + out + '.png'
        #plt.show()
        plt.savefig(png_file)
        plt.clf()
        count += 365
