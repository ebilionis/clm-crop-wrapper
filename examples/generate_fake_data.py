#!/bin/env python
"""
Generate fake data to test if we solve the right way.

TODO: We should add noise.
"""


import numpy as np
import os
#from netCDF4 import Dataset
import cPickle as pickle
import pandas as pd
import _clm_surrogate


NUMBER_OF_SECS_PER_DAY = 24 * 60 * 60

if __name__ == '__main__':
#    datafile = 'clm_crop_0_date=2004_site=bondvilleIL_res=1pt_1pt_compset=I_2000_CN_data.h5'
#    store = pd.HDFStore(datafile)
    # The inputs
#    idx = 4
#    in_df = store['input']
#    in_names = ['leafcn', 'fleafcn', 'livewdcn', 'fstemcn',
#                'frootcn', 'organcn']
#    x = []
#    for in_name in in_names:
#        x.append(in_df[in_name][idx])
#    x = np.array(x)
#    out_df = store['output/s_%s' % str(idx).zfill(9)]
    #doy = np.array([146, 153, 160, 167, 174, 181, 188, 195, 202, 209, 216,
    #                223, 230, 244, 251])
    with open('../data/clm_crop_surrogate_logprior_rvm.pickle', 'rb') as fd:
        surrogate = pickle.load(fd)
    x = np.array([14., 72., 43., 121., 39., 66.]),
    np.savetxt('SurFakeInputs.dat', x)
    doy = np.arange(365, dtype='i')
    leafc = surrogate.evaluate_original_component('LEAFC', x)
    stemc = surrogate.evaluate_original_component('STEMC', x)
    organc = surrogate.evaluate_original_component('ORGANC', x)
    tlai = surrogate.evaluate_original_component('TLAI', x)
    gpp_nee_doy = np.arange(365)
    gpp = surrogate.evaluate_original_component('GPP', x)
    nee = surrogate.evaluate_original_component('NEE', x)

    data = {}
    data['year'] = 2004
    data['LEAFC'] = {'data': leafc, 'doy': doy}
    data['STEMC'] = {'data': stemc, 'doy': doy}
    data['ORGANC'] = {'data': organc, 'doy': doy}
    data['TLAI'] = {'data': tlai, 'doy': doy}
    data['GPP'] = {'data': gpp, 'doy': gpp_nee_doy}
    data['NEE'] = {'data': nee, 'doy': gpp_nee_doy}
    out_filename = os.path.abspath('../data/SurFakeOutputs.pickle')
    print 'Writing:', out_filename
    with open(out_filename, 'wb') as fd:
        pickle.dump(data, fd)
    in_filename = os.path.abspath('../data/SurFakeInputs.pickle')
    np.savetxt(in_filename, x)
