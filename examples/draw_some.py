import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import clm
import _clm_surrogate


if __name__ == '__main__':
    output_names = ['LEAFC', 'TLAI', 'ORGANC', 'STEMC', 'GPP', 'NEE']
    y_con = np.loadtxt('control_out.dat')
    y_cal = np.loadtxt('calibrated_out.dat')
    with open('../data/USBo12004_observed.pickle', 'rb') as fd:
        y_obs = pickle.load(fd)
    count = 0
    for out in output_names:
        idx = np.arange(count, count + 365)
        d_con = y_con[idx]
        d_cal = y_cal[idx]
        if out == 'GPP' or out == 'NEE':
            d_con = clm.get_moving_average(d_con, 30)
            d_cal = clm.get_moving_average(d_cal, 30)
        plt.plot(d_con, 'b', linewidth=2)
        plt.plot(d_cal, 'r', linewidth=2)
        plt.plot(y_obs[out]['doy'], y_obs[out]['data'], 'ko', markersize=5)
        plt.xlabel('doy', fontsize=16)
        plt.ylabel(out, fontsize=16)
        plt.legend(['Control', '\'Calibrated\'', 'Observed'], loc='best')
        png_file = out + '.png'
        plt.savefig(png_file)
        plt.clf()
        count += 365
