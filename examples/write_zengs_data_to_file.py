#!/bin/env python
"""
Convert the hand written data provided by Zeng to a dictionary and then pickle it.
"""


import numpy as np
import os
from netCDF4 import Dataset
import cPickle as pickle


NUMBER_OF_SECS_PER_DAY = 24 * 60 * 60

if __name__ == '__main__':
    # Leaf carbon
    leafc = np.array([5.56, 10.69, 13.81, 47.31, 67.03, 93.56, 111.54,
                      156.76, 195.64, 210.74, 199.91, 208.45, 164.25,
                      96.88, 0.00]) * 0.45
    # Stem carbon
    stemc = np.array([2.72, 5.28, 3.59, 37.46, 63.11, 99.58, 144.83,
                      218.97, 319.80, 366.94, 384.68, 414.53, 361.51,
                      341.40, 241.14]) * 0.45
    # Organ carbon
    organc= np.array([0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 2.03,
                      18.55, 64.44, 146.87, 250.52, 310.25, 603.14,
                      568.55]) * 0.45
    # Leaf aera index
    tlai = np.array([0.1, 0.4, 0.68, 1.34, 2.02, 2.77, 4.26, 5.90, 6.34,
                     6.27, 6.02, 4.91, 5.05, 3.18, 1.28])
    # Day of the year that corresponds to the measurments above
    doy = np.array([146, 153, 160, 167, 174, 181, 188, 195, 202, 209, 216,
                    223, 230, 244, 251])
    data = {}
    data['year'] = 2004
    data['LEAFC'] = {'data': leafc, 'doy': doy}
    data['STEMC'] = {'data': stemc, 'doy': doy}
    data['ORGANC'] = {'data': organc, 'doy': doy}
    data['TLAI'] = {'data': tlai, 'doy': doy}
    # Now read GPP and NEE
    nc_gpp_nee_file = os.path.abspath('../data/USBo12004_L4_d.nc')
    with Dataset(nc_gpp_nee_file, 'r') as fd:
        gpp = fd.variables['GPP_or_MDS'][:] / NUMBER_OF_SECS_PER_DAY
        nee = fd.variables['NEE_or_fMDS'][:] / NUMBER_OF_SECS_PER_DAY
        gpp_nee_doy = fd.variables['DoY'][:]
    data['GPP'] = {'data': gpp, 'doy': gpp_nee_doy}
    data['NEE'] = {'data': nee, 'doy': gpp_nee_doy}
    out_filename = os.path.abspath('../data/USBo12004_observed.pickle')
    print 'Writing:', out_filename
    with open(out_filename, 'wb') as fd:
        pickle.dump(data, fd)
