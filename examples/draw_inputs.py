import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import clm
import _clm_surrogate
import pysmc as ps


if __name__ == '__main__':
    input_names = ['leafcn', 'fleafcn', 'livewdcn', 'fstemcn', 'frootcn',
                   'organcn']
    db = ps.DataBase.load('new_sur_db.pickle')
    real_in = np.loadtxt('../data/SurFakeInputs.pickle')
    count = 0
    for name in input_names:
        plt.hist(db.particle_approximations[0].input[:, count],
                 weights=db.particle_approximations[0].weights,
                 normed=True)
        plt.hist(db.particle_approximation.input[:, count],
                 weights=db.particle_approximation.weights,
                 normed=True)
        plt.plot(real_in[count], 0, 'or', markersize=10)
        plt.xlabel(name, fontsize=16)
        plt.ylabel('p(%s)' % name, fontsize=16)
        plt.legend(['True value', 'Prior', 'Posterior'], loc='best')
        png_file = 'self_reduced_input_' + name + '.png'
        #plt.show()
        count += 1
        plt.savefig(png_file)
        plt.clf()
