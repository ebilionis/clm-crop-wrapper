"""
This is a PyMC implementation of the likelihood model used by Zeng 2013.
"""


import pymc as pm
import pandas as pd
import numpy as np
if __name__ == '__main__':
    import sys
    import os
    sys.path.insert(0, '..')
import clm
import traceback


def make_model(clm_wrapper, observed_data):
    """
    Make the PyMC model.
    """
    input = pm.Normal('input',
                      mu=np.array([25., 65., 50., 130., 42., 60.]),
                      tau=np.array([1. / 3.90625, 1. / 26.4063,
                                         1. / 15.625, 1. / 105.625,
                                         1. / 11.025, 1. / 22.5]))

    # Filter the observed data into what we actually observe
    obs_max_LEAFC = max(observed_data['LEAFC']['data'])
    obs_max_TLAI = max(observed_data['TLAI']['data'])
    obs_max_ORGANC = max(observed_data['ORGANC']['data'])
    obs_max_STEMC = max(observed_data['STEMC']['data'])
    obs_mov_ave_GPP = clm.get_moving_average(observed_data['GPP']['data'],
                                         window=30)
    obs_max_GPP = max(obs_mov_ave_GPP)
    obs_mov_ave_NEE = clm.get_moving_average(observed_data['NEE']['data'],
                                         window=30)
    obs_min_NEE = min(obs_mov_ave_NEE)
    # The calculation of the slopes should probably change to
    # something less error prone
    obs_slope_LEAFC = ((observed_data['LEAFC']['data'][9]
                        - observed_data['LEAFC']['data'][2]) /
                        (observed_data['LEAFC']['doy'][9]
                         - observed_data['LEAFC']['doy'][2]))
    obs_slope_STEMC = ((observed_data['STEMC']['data'][11]
                        - observed_data['STEMC']['data'][2]) /
                        (observed_data['STEMC']['doy'][11]
                         - observed_data['STEMC']['doy'][2]))
    obs_slope_GPP = ((obs_mov_ave_GPP[180] - obs_mov_ave_GPP[160]) / 20.)
    obs_slope_NEE = ((obs_mov_ave_NEE[180] - obs_mov_ave_NEE[160]) / 20.)

    # The output of the forward model
    @pm.deterministic(plot=False)
    def model_output(value=None, input=input,
                     clm_wrapper=clm_wrapper):
        clm_wrapper.input.set_from_array(input)
        try:
           clm_wrapper.run()
        except ValueError as e:
            return np.zeros(clm_wrapper.num_output)
        return clm_wrapper.output.get_as_array()

    # The gamma parameter controlling SMC
    gamma = 1.

    # The variance of observing the max_LEAFC variable
    tau_max_LEAFC = pm.Exponential('tau_max_LEAFC', beta=1.)

    # The maximum of LEAFC
    @pm.stochastic(observed=True)
    def max_LEAFC(value=obs_max_LEAFC, output=model_output,
                  tau_max_LEAFC=tau_max_LEAFC, gamma = gamma,
                  clm_wrapper=clm_wrapper):
        try:
            LEAFC = clm_wrapper.output.get_variable_from_array('LEAFC', output)
        except:
            return -np.inf
        return gamma * pm.normal_like(value, mu=max(LEAFC),
                                      tau=tau_max_LEAFC)

    # The variance of observing the max_TLAI variable
    tau_max_TLAI =  pm.Exponential('tau_max_TLAI', beta=1.)

    # The leaf area index
    @pm.stochastic(observed=True)
    def max_TLAI(value=obs_max_TLAI, output=model_output,
                 tau_max_TLAI=tau_max_TLAI, gamma=gamma,
                 clm_wrapper=clm_wrapper):
        try:
            TLAI = clm_wrapper.output.get_variable_from_array('TLAI', output)
        except:
            return -np.inf
        return gamma * pm.normal_like(value, mu=max(TLAI),
                                      tau=tau_max_TLAI)

    # The variance of observing the max_ORGANC variable
    tau_max_ORGANC = pm.Exponential('tau_max_ORGANC', beta=1.)

    # The organ carbon
    @pm.stochastic(observed=True)
    def max_ORGANC(value=obs_max_ORGANC, output=model_output,
                   tau_max_ORGANC=tau_max_ORGANC, gamma=gamma,
                   clm_wrapper=clm_wrapper):
        try:
            ORGANC = clm_wrapper.output.get_variable_from_array('ORGANC', output)
        except:
            return -np.inf
        return gamma * pm.normal_like(value, mu=max(ORGANC),
                                      tau=tau_max_ORGANC)

    # The variance of observing the max_STEMC variable
    tau_max_STEMC = pm.Exponential('tau_max_STEMC', beta=1.)

    # The stem carbon
    @pm.stochastic(observed=True)
    def max_STEMC(value=obs_max_STEMC, output=model_output,
                  tau_max_STEMC=tau_max_STEMC, gamma=gamma):
        try:
            STEMC = clm_wrapper.output.get_variable_from_array('STEMC', output)
        except:
            return -np.inf
        return gamma * pm.normal_like(value, mu=max(STEMC),
                                      tau=tau_max_STEMC)

    # The moving average of GPP

    @pm.deterministic(plot=False)
    def mov_ave_GPP(value=None, output=model_output, clm_wrapper=clm_wrapper):
        try:
            GPP = clm_wrapper.output.get_variable_from_array('GPP', output)
        except:
            return np.zeros(365)
        return clm.get_moving_average(GPP, window=30)

    # The variance of observing the max_GPP variable
    tau_max_GPP = pm.Exponential('tau_max_GPP', beta=1.)

    # The GPP
    @pm.stochastic(observed=True)
    def max_GPP(value=obs_max_GPP, mov_ave_GPP=mov_ave_GPP,
                tau_max_GPP=tau_max_GPP, gamma=gamma):
        if mov_ave_GPP.sum() == 0.:
            return -np.inf
        return gamma * pm.normal_like(value, mu=max(mov_ave_GPP),
                                      tau=tau_max_GPP)

    # The variance of observing the max_NEE variable
    tau_max_NEE = pm.Exponential('tau_max_NEE', beta=1.)

    # The moving average of NEE
    @pm.deterministic(plot=False)
    def mov_ave_NEE(value=None, output=model_output, clm_wrapper=clm_wrapper):
        try:
            NEE = clm_wrapper.output.get_variable_from_array('NEE', output)
        except:
            return np.zeros(365)
        return clm.get_moving_average(NEE, window=30)

    # The NEE
    @pm.stochastic(observed=True)
    def min_NEE(value=obs_min_NEE, mov_ave_NEE=mov_ave_NEE,
                tau_max_NEE=tau_max_NEE, gamma=gamma):
        if mov_ave_NEE.sum() == 0.:
            return -np.inf
        return gamma * pm.normal_like(value, mu=min(mov_ave_NEE),
                                      tau=tau_max_NEE)

    # Now the slopes
    # The slope is computed on the doy that GDDFRAC[doy] == gddfrac_pt
    gddfrac_pt = 0.3

    # The above mentioned doy
    @pm.deterministic(plot=False)
    def gddfrac_doy(value=None, output=model_output, clm_wrapper=clm_wrapper):
        if output.sum() == 0.:
            return 0
        return clm.find_index_data_exceeds_value(clm_wrapper.output.get_variable_from_array('GDDFRAC', output),
                                                 gddfrac_pt)

    # The variance of observing the slope of LEAFC
    tau_slope_LEAFC = pm.Exponential('tau_slope_LEAFC', beta=1.)

    # The slope of LEAFC
    @pm.stochastic(observed=True)
    def slope_LEAFC(value=obs_slope_LEAFC, gddfrac_doy=gddfrac_doy,
                    output=model_output,
                    tau_slope_LEAFC=tau_slope_LEAFC, gamma=gamma,
                    clm_wrapper=clm_wrapper):
        try:
            LEAFC = clm_wrapper.output.get_variable_from_array('LEAFC', output)
        except:
            return -np.inf
        s = clm.get_slope_at_index(LEAFC, gddfrac_doy, 30)[0]
        return gamma * pm.normal_like(value, mu=s,
                                      tau=tau_slope_LEAFC)

    # The variance of observing the slope of STEMC
    tau_slope_STEMC = pm.Exponential('tau_slope_STEMC', beta=1.)

    # The slope of STEMC
    @pm.stochastic(observed=True)
    def slope_STEMC(value=obs_slope_STEMC, gddfrac_doy=gddfrac_doy,
                    output=model_output,
                    tau_slope_STEMC=tau_slope_STEMC, gamma=gamma,
                    clm_wrapper=clm_wrapper):
        try:
            STEMC = clm_wrapper.output.get_variable_from_array('STEMC', output)
        except:
            return -np.inf
        s = clm.get_slope_at_index(STEMC, gddfrac_doy, 30)[0]
        return gamma * pm.normal_like(value, mu=s,
                                      tau=tau_slope_STEMC)

    # The variance of observing the slope of GPP
    tau_slope_GPP = pm.Exponential('tau_slope_GPP', beta=1.)

    # The slope of GPP
    @pm.stochastic(observed=True)
    def slope_GPP(value=obs_slope_GPP, gddfrac_doy=gddfrac_doy,
                  mov_ave_GPP=mov_ave_GPP,
                  tau_slope_GPP=tau_slope_GPP, gamma=gamma):
        if mov_ave_GPP.sum() == 0.:
            return -np.inf
        s = clm.get_slope_at_index(mov_ave_GPP, gddfrac_doy, 30)[0]
        return gamma * pm.normal_like(value, mu=s,
                                      tau=tau_slope_GPP)

    # The variance of observing the slope of NEE
    tau_slope_NEE = pm.Exponential('tau_slope_NEE', beta=1.)

    # The slope of NEE
    @pm.stochastic(observed=True)
    def slope_NEE(value=obs_slope_NEE, gddfrac_doy=gddfrac_doy,
                  mov_ave_NEE=mov_ave_NEE,
                  tau_slope_NEE=tau_slope_NEE, gamma=gamma):
        if mov_ave_NEE.sum() == 0.:
            return -np.inf
        s = clm.get_slope_at_index(mov_ave_NEE, gddfrac_doy, 30)[0]
        return gamma * pm.normal_like(value, mu=s,
                                      tau=tau_slope_NEE)


    return locals()


if __name__ == '__main__':
    import os
    import sys
    import imp
    import cPickle as pickle
    sys.path.insert(0, os.path.abspath('..'))
    import clm

    case_name = os.path.abspath('../tests/clm_crop_site=bondvilleIL_res=1pt_1pt_compset=I_2000_CN_pool.pickle')
    for i in range(256):
        with open(case_name, 'rb') as fd:
            case = pickle.load(fd)['clones'][i]
        prior_model_file = os.path.abspath('lognormal_prior_model.py')
        prior_model = imp.load_source('', prior_model_file)
        clm_wrapper = clm.Wrapper(case, free_params=prior_model.free_params)
        observed_data_file = os.path.abspath('../data/USBo12004_observed.pickle')
        with open(observed_data_file, 'rb') as fd:
            observed_data = pickle.load(fd)
        try:
            model = pm.Model(make_model(clm_wrapper, observed_data))
            print i, model.logp
        except Exception as e:
            print str(e)
            traceback.print_exc()
            print clm_wrapper.input.get_as_dict()
            quit()
#        print model.stochastics
#        print model.deterministics
