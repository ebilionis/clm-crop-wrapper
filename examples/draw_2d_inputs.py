import numpy as np
import matplotlib.pyplot as plt
import cPickle as pickle
import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import clm
import _clm_surrogate
import pysmc as ps


if __name__ == '__main__':
    input_names = ['leafcn', 'fleafcn', 'livewdcn', 'fstemcn', 'frootcn',
                   'organcn']
    db = ps.DataBase.load('new_sur_db.pickle')
    real_in = np.loadtxt('../data/SurFakeInputs.pickle')
    num_inputs = len(input_names)
    for i in xrange(num_inputs):
        for j in xrange(i + 1, num_inputs):
            plt.hist2d(db.particle_approximations[0].input[:, i],
                       db.particle_approximations[0].input[:, j],
                       weights=db.particle_approximations[0].weights,
                       normed=True,
                       bins=64)
            plt.xlabel(input_names[i], fontsize=16)
            plt.ylabel(input_names[j], fontsize=16)
            png_file = 'self_reduced_input_' + input_names[i] + '_'
            png_file += input_names[j] + '.png'
            plt.show()
            #plt.savefig(png_file)
            #plt.clf()
