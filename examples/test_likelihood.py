"""
Testing the likelihood.
"""

import pymc as pm
import numpy as np
import matplotlib.pyplot as plt


def make_model():
    #max_value = 25.
    #noise_level = 0.5
    #min_tau = 1. / (noise_level * max_value) ** 2
    #alpha = pm.Lognormal('alpha', mu=np.log(max_value/noise_level), tau=1.)
    #beta = pm.Lognormal('beta', mu=np.log(max_value), tau=1.)
    #tau = pm.Gamma('tau', alpha=20./5.7, beta=20.)
    #tau = pm.Gamma('tau', alpha=alpha, beta=beta)
    #tau = pm.Exponential('tau', beta=5.7)
    return locals()


if __name__ == '__main__':
    model = pm.Model(make_model())
    num_samples = 10000
    alphas = []
    betas = []
    taus = []
    sigmas = []
    for i in xrange(num_samples):
        model.draw_from_prior()
        #alphas.append(model.alpha.value)
        #betas.append(model.beta.value)
        taus.append(model.tau.value)
        #if model.tau.value == 0.:
        #    print model.tau.value
        sigmas.append(np.sqrt(1. / model.tau.value))
    #print np.max(taus)
    plt.hist(sigmas, bins=100)
    plt.show()
