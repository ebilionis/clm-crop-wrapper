#!/bin/env python
"""
Generate fake data to test if we solve the right way.

View the generated data.
"""


import numpy as np
import os
from netCDF4 import Dataset
import cPickle as pickle
import pandas as pd
import matplotlib.pyplot as plt


NUMBER_OF_SECS_PER_DAY = 24 * 60 * 60

if __name__ == '__main__':
    datafile = 'clm_crop_site=bondvilleIL_res=1pt_1pt_compset=I_2000_CN_data.h5'
    store = pd.HDFStore(datafile)
    # The inputs
    idx = 4
    in_df = store['input']
    in_names = ['leafcn', 'fleafcn', 'livewdcn', 'fstemcn',
                'frootcn', 'organcn']
    x = []
    for in_name in in_names:
        x.append(in_df[in_name][idx])
    x = np.array(x)
    for idx in range(1000):
        out_df = store['output/s_%s' % str(idx).zfill(9)]
        plt.plot(pd.rolling_mean(out_df['NEE'], window=30), 'r')
    plt.show()
    #doy = np.array([146, 153, 160, 167, 174, 181, 188, 195, 202, 209, 216,
    #                223, 230, 244, 251])

