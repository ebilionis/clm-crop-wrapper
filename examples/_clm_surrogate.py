"""
A CLM surrogate.

We will have a different surrogate for each CLM output.

The output is first reduced using PCA and then fitted.

We will train it with rvm.
The user has to supply the basis functions.

"""


__all__ = ['CLMCropSurrogate']


import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import best.rvm
import best.gpc
import numpy as np


class CLMCropSurrogate(object):

    # The surrogates of the individual components
    _comp_surrogates = None

    # The order of the outputs when you see them as an array
    _output_names = None

    @property
    def comp_surrogates(self):
        """
        Get access to the surrogates of the individual components.
        """
        return self._comp_surrogates

    @property
    def output_names(self):
        """
        Get the output names.
        """
        return self._output_names

    def __init__(self,
                 output_names=['LEAFC', 'TLAI', 'ORGANC', 'STEMC',
                               'GPP', 'NEE']):
        """
        Initialize the object.

        The ``output_names`` variable is important because of the order
        the variable appear. This is the order you will get them as
        if you ask to see them as an array.
        """
        self._comp_surrogates = {}
        self._output_names = output_names

    def train_component(self, name, inputs, outputs, phi):
        """
        Train an output component of CLM.
        """
        # Component surrogate is really a dictionary
        print 'Training', name
        comp_surrogate = {}
        pca = PCA(n_components=.999, whiten=False)
        pca.fit(outputs)
        comp_surrogate['pca'] = pca
        print 'Explained variannce:', pca.explained_variance_ratio_
        # Reduced training data
        r_outputs = pca.transform(outputs)
        # The outputs will be scaled to have unit variance and
        # mean zero
        scaler = StandardScaler().fit(r_outputs)
        comp_surrogate['scaler'] = scaler
        # The scaled reduced outputs
        s_r_outputs = scaler.transform(r_outputs)
        # Construct the design matrix
        PHI = phi(inputs)
        # Use RVM on the data
        rvm = best.rvm.RelevanceVectorMachine()
        rvm.set_data(PHI, s_r_outputs)
        rvm.initialize()
        rvm.train(verbose=False)
        print str(rvm)
        # Get a generalized linear model.
        f = rvm.get_generalized_linear_model(phi)
        comp_surrogate['f'] = f
        self._comp_surrogates[name] = comp_surrogate
        print 'Done'

    def train_components(self, names, inputs, outputs, phis):
        """
        Train all the components.
        """
        for name, output, phi in itertools.izep(names, outputs, phis):
            self.train_component(name, inputs, output, phi)

    def evaluate_component(self, name, inputs):
        """
        Evaluate a particular component at ``inputs``.

        It will return the scaled reduced version of the output.
        """
        return self.comp_surrogates[name]['f'](inputs)

    def __call__(self, inputs):
        """
        Evaluate all components at ``inputs``.

        Return the result as an array.
        """
        res = []
        for out in self.output_names:
            if out == 'GDDFRAC':
                continue
            res.append(self.evaluate_component(out, inputs))
        return np.hstack(res)

    def evaluate_original_component(self, name, inputs):
        """
        Evaluate the original component (full dimensions) at ``inputs``.
        """
        res = self.evaluate_component(name, inputs)
        res = self.comp_surrogates[name]['scaler'].inverse_transform(res)
        res = self.comp_surrogates[name]['pca'].inverse_transform(res)
        return res

    def evaluate_original(self, inputs):
        """
        Evaluate all the original components (full dimensions) at ``inputs``.
        """
        res = []
        for out in self.output_names:
            if out == 'GDDFRAC':
                continue
            res.append(self.evaluate_original_component(out, inputs))
        return np.hstack(res)

    def project_component(self, name, outputs):
        """
        Project the full original component to the reduced/scaled space.
        """
        res = self.comp_surrogates[name]['pca'].transform(outputs)
        res = self.comp_surrogates[name]['scaler'].transform(res)
        return res

    def project(self, outputs):
        """
        Project all the original components to the reduced/scaled space.
        """
        res = []
        count = 0
        for out in self.output_names:
            if out == 'GDDFRAC':
                continue
            n_components = len(self.comp_surrogates[out]['pca'].explained_variance_)
            idx = np.arange(count, count + n_components)
            res.append(self.project_component(out, outputs[idx]))
            count += n_components
        return np.hstack(res)

    def project_from_dict(self, output_dict):
        """
        Project all the original components given as a dictionary to the
        reduced/scaled space.
        """
        res = []
        count = 0
        for out in self.output_names:
            if out == 'GDDFRAC':
                continue
            res.append(self.project_component(out, output_dict[out]['data']))
        return np.hstack(res)

    @property
    def num_output(self):
        """
        Get the number of outputs.
        """
        count = 0
        for out in self.output_names:
            if out == 'GDDFRAC':
                continue
            count += len(self.comp_surrogates[out]['pca'].explained_variance_)
        return count
