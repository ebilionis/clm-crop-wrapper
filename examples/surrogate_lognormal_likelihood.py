"""
This is a PyMC implementation of the likelihood model used by Zeng 2013.
"""


import pymc as pm
import pandas as pd
import numpy as np
if __name__ == '__main__':
    import sys
    import os
    sys.path.insert(0, '..')
import clm
import traceback


def make_model(clm_surrogate, observed_data):
    """
    Make the PyMC model.
    """
    # Get the input dictionary from the wrapper
    x = pm.Normal('input',
                  mu=np.array([25., 65., 50., 130., 42., 60., 0.07]),
                  tau=1. / np.array([9., 9., 9., 9., 9., 9., 0.01]))

    # Get the observed data as an array
    obs_reduced_data = clm_surrogate.project_from_dict(observed_data)

    # The output of the forward model
    @pm.deterministic(plot=False)
    def model_output(value=None, x=x, f=clm_surrogate):
        y = f(x)
        return y

    # Pick a prior for the variances
    #sigma = pm.Exponential('tau', beta=0.1 * np.ones(clm_surrogate.num_output))
    sigma = np.ones(clm_surrogate.num_output) * 1e-1

    # The likelihood of the observed data
    @pm.stochastic(observed=True)
    def output(value=obs_reduced_data, y=model_output, sigma=sigma,
                 gamma=1.):
        return gamma * pm.normal_like(value, mu=y, tau=1. / (sigma ** 2))

    return locals()


if __name__ == '__main__':
    import os
    import sys
    import imp
    import cPickle as pickle
    sys.path.insert(0, os.path.abspath('..'))
    import clm
    import pymc as pm
    import pysmc as ps

    surrogate_file = '../data/clm_crop_surrogate_logprior_rvm.pickle'
    observations_file = '../data/SurFakeOutputs.pickle'

    with open(surrogate_file, 'rb') as fd:
        surrogate = pickle.load(fd)

    with open(observations_file, 'rb') as fd:
        obs = pickle.load(fd)

    model = make_model(surrogate, obs)

    # Check if mpi is on or off
    import mpi4py.MPI as mpi
    rank = mpi.COMM_WORLD.Get_rank()
    size = mpi.COMM_WORLD.Get_size()

    # Construct the SMC sampler
    my_mcmc = pm.MCMC(model)
    my_mcmc.use_step_method(ps.GaussianMixtureStep, model['x'])
#    my_mcmc.use_step_method(ps.GaussianMixtureStep, model['sigma'])
    smc_sampler = ps.SMC(my_mcmc,
                             num_particles=1024,
                             num_mcmc=1,
                             verbose=4,
                             mpi=mpi,
                             gamma_is_an_exponent=True,
                             db_filename='new_sur_db.pickle',
                             update_db=True,
                             ess_reduction=0.7,
                             ess_threshold=0.2,
                             gamma_name='gamma')
    # Initialize if required
    #if not os.path.exists('sur_db.pickle'):
    smc_sampler.initialize(0.)
    smc_sampler.move_to(1.)
