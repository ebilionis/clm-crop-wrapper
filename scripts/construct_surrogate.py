"""
Construct a surrogate from CLM-Crop simulations.
"""

import pandas as pd
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import best.rvm
import best.maps
import best.random
import best.gpc
import numpy as np
import cPickle as pickle
import os
import matplotlib.pyplot as plt
from optparse import OptionParser
import sys
import scipy.stats
import math
from clm import CLMCropSurrogate
import warnings
warnings.filterwarnings("ignore")


def get_moving_average(series, window):
    """
    :param series:      The time series to average.
    :type series:       1D numpy.ndarray
    :param window:      The window size
    :type window:       int
    """
    # Repeat the last value window / 2 times
    augmented_series = np.hstack([series, np.ones(window / 2) * series[-1]])
    return pd.rolling_mean(augmented_series, window=window, center=True,
                           min_periods=0)[:series.shape[0]]


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--data', dest='data_file', type='string',
                      help='Specify the objected data (HDF5-Pandas format).')
    parser.add_option('-n', '--num-train', dest='num_train', type='int',
                      help='Specify the number of samples to use.'
                           'The default is to use 70\% and use the remaining'
                           ' 30\% for tests.')
    parser.add_option('-p', '--degree', dest='degree', type='int',
                      default=7,
                      help='Specify the degree of gPC [default: %default].')
    (options, args) = parser.parse_args()

    if options.data_file is None:
        sys.stderr.write('You have to specify the data file.\n')
        sys.exit(1)
    # Get the prefix to be used
    prefix = os.path.splitext(options.data_file)[0]
    # Load the simulation data
    store = pd.HDFStore(options.data_file)
    # Read the names of the inputs contained in the file
    input_names = list(store['input'].columns)
    print 'Inputs:', input_names
    print 'Constructing generalized Polynomial Chaos basis...'
    rv_dict = {
        'leafcn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(25.)),
        'fleafcn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(65.)),
        'livewdcn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(50.)),
        'fstemcn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(130.)),
        'frootcn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(42.)),
        'graincn':scipy.stats.lognorm(math.sqrt(1. / 9.), loc=math.log(60.)),
        'slatop':scipy.stats.uniform(loc=0.05, scale=0.04)}
    # Make sure they are properly ordered
    rv_list = []
    for input_name in input_names:
        rv_list.append(rv_dict[input_name])
    # Construct multidimensional random vector
    rv = best.random.RandomVectorIndependent(rv_list)
    # Construct the basis
    phi = best.gpc.ProductBasis(rv=rv, degree=options.degree)

    # Read the output names from the file
    output_names = list(store['output/s_%s' % str(0).zfill(9)].columns)
    print 'Outputs:', output_names

    # Initiallize the surrogate
    surrogate = CLMCropSurrogate(output_names=output_names)

    # Loop over the outputs and construct surrogates for each one
    for out_name in output_names:
        print 'Doing output:', out_name
        # Load the data (write pickled file for quicker reading next time)
        out_file = prefix + '_' + out_name + '.pcl'
        if os.path.exists(out_file):
            print 'Reading:', out_file
            with open(out_file, 'rb') as fd:
                data = pickle.load(fd)
        else:
            data = []
            count = 0
            while True:
                try:
                    print 'Reading object', count
                    row = store['output/s_%s' % str(count).zfill(9)][out_name]
                    row = np.array(row)
                    if out_name == 'GPP' or out_name == 'NEE':
                        row = get_moving_average(row, 30)
                    data.append(row)
                    count += 1
                except KeyError:
                    print 'Done'
                    break
            data = np.vstack(data)
            print 'Writing:', out_file
            with open(out_file, 'wb') as fd:
                pickle.dump(data, fd)
        # Do not use all the data
        if options.num_train is None:
            num_train = int(0.7 * data.shape[0])
        else:
            assert options.num_train <= data.shape[0]
            num_train = options.num_train
        print 'Using', num_train, 'observations for surrogate.'
        train_inputs = np.array(store['input'])[:num_train, :]
        train_outputs = data[:num_train, :]
        surrogate.train_component(out_name, train_inputs, train_outputs, phi)

    # Save the surrogate to a file
    surrogate_file = prefix + '_surrogate.pcl'
    with open(surrogate_file, 'wb') as fd:
        pickle.dump(surrogate, fd, protocol=pickle.HIGHEST_PROTOCOL)
