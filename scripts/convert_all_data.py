"""
Convert all the output data to a numpy array.
"""

import pandas as pd


import numpy as np
import cPickle as pickle
import os
import matplotlib.pyplot as plt


if __name__ == '__main__':
    store = pd.HDFStore(
 'clm_crop_0_date=2004_site=bondvilleIL_res=1pt_1pt_compset=I_2000_CN_data.h5')
    data_out_file = 'clm_crop_data.pickle'
    data = []
    count = 0
    while True:
        try:
            print 'Reading object', count
            row = np.array(store['output/s_%s' % str(count).zfill(9)])
            row = np.atleast_2d(row.flatten(order='F'))
            data.append(np.array(row))
            count += 1
        except:
            print 'Done'
            break
    data = np.vstack(data)
    with open(data_out_file, 'wb') as fd:
        pickle.dump(data, fd)
