#!/bin/env python
"""
A driver script for solving inverse problems.
"""


from common import *
import warnings
import numpy as np
import traceback


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_pool_option(parser)
    add_num_particles_option(parser)
    add_num_mcmc_option(parser)
    add_likelihood_model_option(parser)
    add_prior_model_option(parser)
    add_db_filename_option(parser)
    add_observed_data_option(parser)
    add_continue_option(parser)
    add_initial_gamma_option(parser)
    add_target_gamma_option(parser)
    add_gamma_name_option(parser)
    add_gamma_is_an_exponent_option(parser)
    add_use_mpi_option(parser)

    # Parse the options
    (options, args) = parser.parse_args()

    # Check if mpi is on or off
    mpi, rank, size = get_mpi(options.use_mpi)

    # Sanity check
    check_option(options, 'case_pool', mpi=mpi)
    check_option(options, 'num_particles', mpi=mpi)
    check_option(options, 'prior_model', mpi=mpi)
    check_option(options, 'likelihood_model', mpi=mpi)
    check_option(options, 'db_filename', mpi=mpi)
    check_option(options, 'observed_data', mpi=mpi)

    # Load pymc and pysmc
    pm = load_pymc()
    ps = load_pysmc()

    # Load the data
    case_pool = initialize_pickled_object(options.case_pool, 'case pool',
                                          mpi=mpi)
    my_case = case_pool['clones'][rank]
    observed_data = initialize_pickled_object(options.observed_data,
                                              'observed data', mpi=mpi)
    prior_model = initialize_model(options.prior_model, 'prior model', mpi=mpi)
    free_params = prior_model.free_params
    likelihood_model = initialize_model(options.likelihood_model,
                                        'likelihood model', mpi=mpi)
    # Construct the wrapper
    my_wrapper = clm.Wrapper(my_case, free_params=free_params)
    # Construct the probability model
    my_model = likelihood_model.make_model(my_wrapper, observed_data)

    # Solve it!
    try:
        # Construct the SMC sampler
        my_mcmc = pm.MCMC(my_model)
        my_mcmc.use_step_method(ps.GaussianMixtureStep, my_model['input'])
        smc_sampler = ps.SMC(my_mcmc,
                             num_particles=options.num_particles,
                             num_mcmc=options.num_mcmc,
                             verbose=4,
                             mpi=mpi,
                             gamma_is_an_exponent=options.gamma_is_an_exponent,
                             db_filename=options.db_filename,
                             update_db=True,
                             ess_reduction=0.7,
                             ess_threshold=0.2,
                             gamma_name=options.gamma_name)
        # Initialize if required
        if not options.cont:
            smc_sampler.initialize(options.initial_gamma)
        smc_sampler.move_to(options.target_gamma)
    except Exception as e:
        traceback.print_exc()
        if options.use_mpi:
            mpi.COMM_WORLD.Abort(1)
