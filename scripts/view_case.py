#!/bin/env python
"""
This script prints information about a pickled case. You can either print all
the available information about the case or simply request a particular
attribute (via the '-a (--attribute-list)' option).

"""


from common import *


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    # Create a small description for the script
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_option(parser)
    add_attribute_option(parser)
    # Parse the options
    (options, args) = parser.parse_args()
    # Sanity check
    check_option(options, 'pickled_case')
    # Open the file
    with open(options.pickled_case, 'rb') as fd:
        case = pickle.load(fd)
    # Parse the list of attributes
    if options.attribute_list is not None:
        attributes = parse_attributes(options, case)
        for att in attributes:
            print att, ':'
            print '\t', getattr(case, att)
    else:
        print str(case)
