#!/bin/env python
"""
A script that submits inverse jobs.
"""


from common import *
import subprocess
import numpy as np
import time


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_pool_option(parser)
    add_just_check_option(parser)
    add_pbs_options(parser)
    add_num_particles_option(parser)
    add_num_mcmc_option(parser)
    add_likelihood_model_option(parser)
    add_prior_model_option(parser)
    add_db_filename_option(parser)
    add_observed_data_option(parser)
    add_continue_option(parser)
    add_initial_gamma_option(parser)
    add_target_gamma_option(parser)
    add_gamma_name_option(parser)
    add_gamma_is_an_exponent_option(parser)
    parser.add_option('--num-tasks', dest='num_tasks', type='int',
                      default=8,
                      help='Specify the number of MPI tasks.'
                           ' [default: %default]')
    parser.add_option('--num-submissions', dest='num_submissions',
                      type='int', default=1,
                      help='Specify the number of submissions.'
                           ' [default: %default]')
    parser.add_option('--depend', dest='depend', type='str',
                      help='This should be a valid job-id. If it is specified,'
                           ' then this script will submit jobs that start'
                           ' after the job with this job-id has been'
                           ' completed.')
    parser.add_option('--job-name', dest='job_name', type='str',
                      default='inverse',
                      help='Give me a job name. [default: %default]')

    (options, args) = parser.parse_args()

    # Sanity check
    check_option(options, 'case_pool')
    check_option(options, 'num_particles')
    check_option(options, 'prior_model')
    check_option(options, 'likelihood_model')
    check_option(options, 'db_filename')
    check_option(options, 'observed_data')

    # Do the job
    if options.time is None:
        walltime = "01:00:00"
    else:
        walltime = options.time

    for i in xrange(options.num_submissions):
        job_name = '%d_%s' % (options.num_particles, options.job_name)
        pbs_script = """
#!/bin/sh
#PBS -A %s
#PBS -N %s
#PBS -l nodes=%d:ppn=%d
#PBS -l walltime=%s
#PBS -j oe


cd $PBS_O_WORKDIR

mpirun -n %d \\
    ../scripts/solve_inverse.py \\
    -n %d \\
    -c  %s \\
    -p %s \\
    -l %s \\
    -d %s \\
    -o %s\
""" % (options.allocation, job_name, options.num_tasks / options.ppn, options.ppn, walltime,
       options.num_tasks, options.num_particles, options.case_pool, options.prior_model,
       options.likelihood_model, options.db_filename, options.observed_data)
        if i > 0 or options.cont:
            pbs_script += """ \\
    --continue\
"""
        pbs_script += """ \\
    --use-mpi
"""
        pbs_file = job_name + '_' + str(i) + '.pbs'
        if not options.just_check:
            cmd = ['qsub']
            if i == 0 and options.depend is not None:
                cmd += ['-W', 'depend=afterany:%s' % options.depend]
            if i > 0:
                cmd += ['-W', 'depend=afterany:%s' % prev_job_id]
            cmd += [pbs_file]
            with open(pbs_file, 'w') as fd:
                fd.write(pbs_script)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            p.wait()
            if not p.returncode == 0:
                raise RuntimeError('I couldn\'t submit job \'%s\'.' % pbs_file)
            job_id = p.stdout.readline().split('.')[0]
            print ' '.join(cmd), '--->', job_id
            prev_job_id = job_id
        else:
            print pbs_script
