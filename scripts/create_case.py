#!/bin/env python
"""
This is simple script for creating, configuring and compiling CLM-crop cases.
It writes a pickled file containing info about cloning and running the case.
The name of the pickled file is there specified directly by appending the
extension 'pickle' to the value of the '-n (--case-name)'. If this option is
not specified, the we are looking at '-p (--case-prefix)' to come up with an
appropriate name for the case and then append to that one the extension 'pickle'.
Note that if the case is already created (configured or built) it will not
be created (configured or built) again. If you wish to actually force the case
to be re-created (configured or built), you should use the '-f (--force)'
option.

"""


from common import *


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    # Create a small description for the script
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    parser.add_option('-c', '--case-dir', dest='case_dir', type='string',
                      help='Provide the case directory to be transformed into'
                           ' the localized case.')
    parser.add_option('-n', '--new-case-dir', dest='new_case_dir',
                      type='string',
                      help='The new case directory.')
    parser.add_option('-o', '--output', dest='output', type='string',
                      help='The output file in which the pickled object goes.')
    parser.add_option('-f', '--force', dest='force', action='store_true',
                      default=False,
                      help='Force the construction of the case.')
    # Parse the options
    (options, args) = parser.parse_args()
    # Sanity check (a name must be specified).
    if options.case_dir is None:
        sys.stderr.write('The case directory must be provided.')
        sys.exit(1)
    if options.new_case_dir is None:
        options.new_case_dir = options.case_dir + '_localized'
    if options.output is None:
        options.output = options.new_case_dir + '.pcl'
    if os.path.isdir(options.new_case_dir) and not options.force:
        print 'New case dir exists!'
        case = clm.Case(options.new_case_dir)
    else:
        case = clm.Case.create_from_clm_case(options.case_dir,
                                             options.new_case_dir)
    print 'Writing:', options.output
    with open(options.output, 'wb') as fd:
        pickle.dump(case, fd, protocol=pickle.HIGHEST_PROTOCOL)
