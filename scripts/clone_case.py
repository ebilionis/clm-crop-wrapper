#!/bin/env python
"""
This scripts allows you to clone a case. This is useful in preparing a parallel
job. All file copying can be done by this script offline.
"""


from common import *


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    # Create a small description for the script
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_option(parser)
    parser.add_option('--range', dest='range',
                      help='Specify a range for the output')
    parser.add_option('-o', '--output', dest='output',
                      help='Specify a file name for the pickled object representing'
                           ' the pool of cases. If you don\'t it will be named'
                           ' by adding the word \'pool\' to the case name.')
    parser.add_option('-f', '--force', dest='force', action='store_true',
                      default=False,
                      help='Force the construction of the case.')
    # Parse the options
    (options, args) = parser.parse_args()
    # Sanity check
    check_option(options, 'pickled_case')
    with open(options.pickled_case, 'rb') as fd:
        case = pickle.load(fd)
    pickle_file = os.path.abspath(options.pickled_case)
    if options.range is None:
        sys.stderr.write('You have to provide a range for the outputs.')
    # Parse the range
    tmp = options.range.split(':')
    r0 = int(tmp[0])
    r1 = int(tmp[1])
    if len(tmp) == 3:
        r2 = tmp[2]
    else:
        r2 = 1
    # Get names of cloned cases
    case_names = [case.case_dir + '_' + str(i)
                  for i in range(r0, r1, r2)]
    # Get the output file
    if options.output is None:
        output = case.case_dir + '_pool.pcl'
    else:
        output = options.output
    # Loop over the cloned cases
    case_pool = {}
    case_pool['base'] = case
    case_pool['clones'] = []
    for case_name in case_names:
        print 'Creating case:', case_name
        if os.path.isdir(case_name) and not options.force:
            print 'Case dir found!'
            new_case = clm.Case(case_name)
        else:
            new_case = case.clone(case_name)
        case_pool['clones'].append(new_case)
    print 'Writing:', output
    with open(output, 'wb') as fd:
        pickle.dump(case_pool, fd, pickle.HIGHEST_PROTOCOL)
