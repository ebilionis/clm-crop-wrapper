"""
Definition of modules and functions that are common to all
scripts.
"""



import os
import sys
import re
import cPickle as pickle
from optparse import OptionParser
import imp
import contextlib
import imp


# Loads the CLM-Crop wrapper module (CLM).
# If you installed it, that's perfect.
# Otherwise, we improvise...
try:
    import clm
except:
    # This is a list of paths that it will try to find the module
    sys.path.insert(0, os.path.abspath('..'))
    sys.path.insert(0, os.path.abspath('.'))
    sys.path.insert(0, os.path.abspath(
                    os.path.join(os.path.split(__file__)[0], '..')))
    try:
        import clm
    except:
        sys.stderr.write('Cannot find the CLM-Crop wrapper module (i.e., clm).'
                         ' Make sure you have properly configured it.\n')
        sys.exit(1)



def print_help(prog_name, script_list):
    """
    Prints the help of the script.
    """
    print __doc__
    print ''
    print 'Usage:', prog_name, 'script script_options'
    print ''
    print 'Options:'
    for name, desc in script_list:
        print '\t{0:10}\t{1:49}'.format(name, desc)
    print '\t{0:10}\t{1:49}'.format('-h, --help', 'Prints this help message')
    print ''
    print 'Help for individual scripts can be obtained by:'
    print '\t', prog_name, 'help script'


def is_script_name(name, script_list):
    """
    Checks if ``name`` is a valid script name.
    """
    for script, desc in script_list:
        if name == script:
            return True
    return False


def check_script_name(name, script_list):
    """
    Checks the script name and exists if it is not found.
    """
    if not is_script_name(name, script_list):
        sys.stderr.write('There is no script named \'%s\'.\n' % name)
        sys.exit(1)


@contextlib.contextmanager
def script_context(script, script_argv):
    sys._argv = sys.argv[:]
    sys.argv = [sys.argv[0] + ' ' + script] + script_argv
    yield
    sys.argv = sys._argv


def run_script_with_args(base_name, script_name, argv, script_list):
    """
    Run the script as if it was the __main__ script and give it
    a particular argument list.
    """
    check_script_name(script_name, script_list)
    source_name = script_name + '_' + base_name
    fp, pathname, description = imp.find_module(source_name)
    with script_context(script_name, argv):
        imp.load_module('__main__', fp, pathname, description)
    sys.exit(0)


def generic_script_main(base_name, script_list, doc):
    """
    A generic main function for the base scripts.
    """
    if len(sys.argv) == 1:
        print doc
        print 'For more details as for help:'
        print '\t', sys.argv[0], '-h'
        sys.exit(0)
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        print_help(sys.argv[0], script_list)
        sys.exit(0)
    if sys.argv[1] == 'help':
        if len(sys.argv) < 3:
            sys.stderr.write('You have to provide a script for help.\n')
            sys.exit(1)
        run_script_with_args(base_name, sys.argv[2], ['-h'], script_list)
    run_script_with_args(base_name, sys.argv[1], sys.argv[2:], script_list)


# Valid delimiters for specifying lists
DELIMITERS = [' ', ',', ':', '.', '@', ';', '!']


# A list of influential system variables with a small description for each one.
SYS_VARS = [('CCSM_ROOT', 'the root of CCSM code'),
            ('CCSM_CASES', 'the place we store new cases'),
            ('CCSM_COMPILE', 'the place where we compile new cases'),
            ('CCSM_RUN', 'the place where new cases are supposed to be run'),
            ('CCSM_DATA', 'the place where we store the usual CCSM data'),
            ('CCSM_EXT_DATA', 'the place where we store user created CCSM data'),
            ('CCSM_ARCHIVE', 'the place where the output of CLM is archived')]


# Some common options
def add_case_option(parser):
    """
    Add to the parser the option that has to do with the
    specification of the pickled version of the casecase.
    """
    parser.add_option('-c', '--case', dest='pickled_case', type='string',
                      help='The pickle file containing the case you want'
                           ' to see.')


def add_force_option(parser):
    """
    Add to the parser the force option.
    """
    parser.add_option('-f', '--force', dest='force',
                      default=False, action='store_true',
                      help='By specifying this option you are forcing the '
                           ' script to execute its action no matter what.'
                           ' That is, it will overwite files etc.')


def add_case_pool_option(parser):
    """
    Add to the parser the pickled case pool option.
    """
    parser.add_option('-c', '--case-pool', dest='case_pool',
                      type='string',
                      help='Specify the pickled case pool.')


def add_num_samples_option(parser):
    """
    Add to the sampler the number of samples option.
    """
    parser.add_option('-s', '--num-samples', dest='num_samples', type='int',
                      help='The number of samples to be taken by the minimum'
                            ' number of cpus.')


def add_prior_model_option(parser):
    """
    Add to the parser the option that has to do with the
    specification of the prior model.
    """
    parser.add_option('-p', '--prior-model', dest='prior_model',
                      help='Set the prior probability model (should be a file'
                           'containing a python module).',
                      type='string')

def add_use_mpi_option(parser):
    """
    Add to the parser the option that has to do with the mpi
    flag.
    """
    parser.add_option('--use-mpi', dest='use_mpi', action='store_true',
                      default=False,
                      help='Turn this on if you want to use MPI to do parallel'
                           ' sampling. [default: %default]')

def add_attribute_option(parser):
    """
    Add to the parser the option that has to do with the specification
    of the attributes list.
    """
    parser.add_option('-a', '--attribute-list', dest='attribute_list',
                      type='string',
                      help='Specify a list of atributes to look at.'
                           ' The list of attributes should be a list of'
                           ' attribute names separated by one of the following'
                           ' delimiters: '
                           + ','.join(['\'%s\'' % d for d in DELIMITERS]) + '.'
                     )


def add_pbs_options(parser):
    """
    Specify the pbs options.
    """
    parser.add_option('--ppn', dest='ppn', type='int',
                      default=8,
                      help='Specify the number of processors per node.'
                           ' [default: %default]')
    parser.add_option('--allocation', dest='allocation', type='string',
                      default='uncertainty-climate',
                      help='Specify the name of the allocation.'
                           ' [default: %default]')
    parser.add_option('--time', dest='time', type='string',
                      help='Specify the maximum time for each run. If None,'
                           ' then I will improvise.')


def add_just_check_option(parser):
    """
    Add to the parser the just check-option.
    """
    parser.add_option('--just-check', dest='just_check', action='store_true',
                      default=False,
                      help='Don\'t actually do anything.'
                           ' Just check what would be run.')


def add_num_particles_option(parser):
    """
    Add to the parser the number of particles option.
    """
    parser.add_option('-n', '--num-particles', dest='num_particles', type='int',
                      help='The number of particles to be used.')


def add_num_mcmc_option(parser):
    """
    Add to the parser the number of mcmc option.
    """
    parser.add_option('-m', '--num-mcmc', dest='num_mcmc', type='int',
                      default=1,
                      help='The number of MCMC steps per gamma. '
                           ' [default: %default]')


def add_likelihood_model_option(parser):
    """
    Add to the parser the likelihood model option.
    """
    parser.add_option('-l', '--likelihood-model', dest='likelihood_model',
                      type='str',
                      help='The likelihood model you would like to use.')


def add_db_filename_option(parser):
    """
    Add to the parser the database filename option.
    """
    parser.add_option('-d', '--db-filename', dest='db_filename',
                      type='str',
                      help='The database file to be used to save the output.'
                           ' If the database filename exists, then we will'
                           ' attempt to initialize the run from its last'
                           ' particle approximation.')


def add_observed_data_option(parser):
    """
    Add to the parser the observed data option.
    """
    parser.add_option('-o', '--observed-data', dest='observed_data',
                      type='str',
                      help='Specify the pickled file containing the observed'
                           ' data.')


def add_continue_option(parser):
    """
    Add to the parser the continue option.
    """
    parser.add_option('--continue', dest='cont', action='store_true',
                      default=False,
                      help='Specify this option if this is a continuation run.')


def add_target_gamma_option(parser):
    """
    Add to the parser the target gamma option.
    """
    parser.add_option('--target-gamma', dest='target_gamma', type='float',
                      default=1.,
                      help='The target gamma you have in mind.'
                           ' [default: %default]')


def add_initial_gamma_option(parser):
    """
    Add to the parser the initial gamma option.
    """
    parser.add_option('--initial-gamma', dest='initial_gamma', type='float',
                      default=0.,
                      help='The gamma you want to start at. This option is not'
                      ' enforced if the \'--continue\' option is specified.'
                      ' [default: %default].')


def add_gamma_name_option(parser):
    """
    Add to the parser the gamma name option.
    """
    parser.add_option('--gamma-name', dest='gamma_name', type='str',
                      default='gamma',
                      help='The actual name of the gamma parameter in the'
                           ' model. [default: %default]\n')


def add_gamma_is_an_exponent_option(parser):
    """
    Add to the parser the gamma is an exponent option.
    """
    parser.add_option('--gamma-is-an-exponent', dest='gamma_is_an_exponent',
                      action='store_true', default=False,
                      help='Specify this if the gamma parameter appears as an'
                           ' exponent in your model. This would make the'
                           ' adaptive algorithm much faster. Of course, you'
                           ' will get garbage if this assumption is not true.'
                           ' [default: %default]')


def check_option(options, name, mpi=None):
    """
    Sanity check for the option ``name``.
    """
    if mpi is not None:
        rank = mpi.COMM_WORLD.Get_rank()
    else:
        rank = 0
    if getattr(options, name) is None:
        if rank == 0:
            sys.stderr.write('You have to specify the %s option.\n' % name)
        if mpi is None:
            sys.exit(1)
        else:
            mpi.COMM_WORLD.barrier()
            mpi.Abort(1)


def parse_attributes(options, case):
    """
    Parse the attribute list.
    """
    del_pattern = '\s*[' + ''.join(DELIMITERS) + ']\s*'
    attributes = re.split(del_pattern, options.attribute_list)
    if attributes is None:
        sys.stderr.write('The attribute list \'%s\' is not a valid one.')
        sys.exit(1)
    all_valid = True
    for att in attributes:
        if not hasattr(case, att):
            sys.stderr.write('Attribute \'%s\' not found.\n' % att)
            all_valid = False
            break
    if not all_valid:
        sys.exit(1)
    return attributes


def print_once(msg, mpi=None):
    """
    Use to print a message once when using mpi.
    """
    if mpi is None:
        sys.stdout.write(msg)
    else:
        rank = mpi.COMM_WORLD.Get_rank()
        if rank == 0:
            sys.stdout.write(msg)
            sys.stdout.flush()
        mpi.COMM_WORLD.barrier()


def print_rank(i, msg):
    """
    Use to print information from a particular rank.
    This is an mpi only function.
    """
    sys.stdout.write('rank %d: %s' % (i, msg))
    sys.stdout.flush()


def signal_fatal_error(err, err_code=1, mpi=None):
    """
    Signal a fatal error. The program will exit.
    """
    sys.stderr.write(err)
    if mpi is None:
        sys.exit(1)
    else:
        mpi.COMM_WORLD.Abort(1)


def mpi_wait(mpi):
    """
    Wait if mpi is not ``None``.
    """
    if mpi is not None:
        mpi.COMM_WORLD.barrier()


def get_mpi(use_mpi):
    """
    Return mpi4py.MPI, rank, size if use_mpi is True.
    Otherwise, return None, 0, 1.
    """
    if use_mpi:
        import mpi4py.MPI as mpi
    else:
        mpi = None
    rank, size = get_rank_size(mpi)
    return mpi, rank, size


def get_rank_size(mpi):
    """
    Return the right rank of size depending on what mpi is.

    If it is ``None``, return 0 and 1 respectively.
    """
    if mpi is None:
        return 0, 1
    else:
        rank = mpi.COMM_WORLD.Get_rank()
        size = mpi.COMM_WORLD.Get_size()
        return rank, size


def initialize_pickled_object(obj_file, obj_name, mpi=None):
    """
    Initialize a pickled object.
    """
    rank, size = get_rank_size(mpi)
    print_once('Initializing the pickled object %s\n' % obj_name, mpi=mpi)
    print_once('-----------------------------------------\n', mpi=mpi)
    if rank == 0:
        try:
            with open(obj_file, 'rb') as fd:
                obj = pickle.load(fd)
        except:
            signal_fatal_error('Problem loading pickled object %s\n' % obj_name,
                               mpi=mpi)
    else:
        obj = None
    if mpi is not None:
        obj = mpi.COMM_WORLD.bcast(obj)
    print_once('Done\n', mpi=mpi)
    print_once('--------------------------\n', mpi=mpi)
    return obj


def initialize_model(model_file, model_name, mpi=None):
    """
    Initialize the ``model_file`` which should be a proper python module.
    """
    rank, size = get_rank_size(mpi)
    print_once('Initializing the %s.\n' % model_name, mpi=mpi)
    print_once('-------------------------\n', mpi=mpi)
    for i in xrange(size):
        if rank == i:
            try:
                model = imp.load_source('', model_file)
            except:
                signal_fatal_error('I couldn\'t load the %s.\n' % model_name,
                                   mpi=mpi)
            print_rank(i, 'initialized %s.\n' % model_name)
        mpi_wait(mpi)
    print_once('Done.\n'
               '-------------------------\n', mpi=mpi)
    return model


def load_pymc():
    """
    Load PyMC without all the fuss with matplotlib.
    """
    devnull = open('/dev/null', 'w')
    oldstdout_fno = os.dup(sys.stdout.fileno())
    oldstderr_fno = os.dup(sys.stderr.fileno())
    os.dup2(devnull.fileno(), 1)
    os.dup2(devnull.fileno(), 2)
    import pymc
    os.dup2(oldstdout_fno, 1)
    os.dup2(oldstderr_fno, 2)
    return pymc


def load_pysmc():
    """
    Load PyMC without all the fuss with matplotlib.
    """
    devnull = open('/dev/null', 'w')
    oldstdout_fno = os.dup(sys.stdout.fileno())
    oldstderr_fno = os.dup(sys.stderr.fileno())
    os.dup2(devnull.fileno(), 1)
    os.dup2(devnull.fileno(), 2)
    import pysmc
    os.dup2(oldstdout_fno, 1)
    os.dup2(oldstderr_fno, 2)
    return pysmc
