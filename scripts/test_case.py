#!/bin/env python
# vi: ft=python
"""
A bunch of performance tests for the code.
"""


import os
import sys
sys.path.insert(0, os.path.split(os.path.abspath(__file__))[0])
import common


TEST_SCRIPT_LIST = [('data-collection', 'A test that measures the time it'
                                        + ' takes to collect data')]


if __name__ == '__main__':
    common.generic_script_main('test_case', TEST_SCRIPT_LIST, __doc__)
