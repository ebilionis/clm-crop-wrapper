#!/bin/env python
"""
This script plots histograms of the prior probability for the inputs.
"""


from common import *
import matplotlib.pyplot as plt


def plot_hist(var, out_dir='.', num_samples=10000):
    """
    Plot histogram for variable ``var``.
    """
    print 'Constructing histogram for:', var['name']
    # Take samples
    samples = []
    for i in xrange(num_samples):
        samples.append(var['pdf'].rand())
    plt.figure()
    plt.hist(samples, bins=num_samples/100)
    plt.xlabel(var['name'], fontsize=16)
    plt.ylabel('p(' + var['name'] + ')', fontsize=16)
    png_file = os.path.join(out_dir, var['name'] + '_prior_hist.png')
    plt.savefig(png_file)


if __name__ == '__main__':
    # Set the options
    parser = OptionParser()
    add_prior_model_option(parser)
    parser.add_option('-o', '--out-dir', dest='out_dir', default='.',
                      help='Specify the output directory. [default: %default]')
    # Parse them
    (options, args) = parser.parse_args()
    # Do a sanity check
    check_option(options, 'prior_model')
    prior_model = imp.load_source('', options.prior_model)
    free_params = prior_model.free_params
    out_dir = os.path.abspath(options.out_dir)
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for var in free_params:
        plot_hist(var, out_dir=out_dir)
