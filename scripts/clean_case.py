#!/bin/env python
"""
This script allows you to clean a case.
"""


from common import *


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    # Create a small description for the script
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_option(parser)
    parser.add_option('--archive', dest='archive', action='store_true',
                      default=False,
                      help='Specify this option if you also want to delete the'
                           ' archive directory of the case.'
                           ' [default: %default]')
    add_just_check_option(parser)
    parser.add_option('--pickle', dest='pickle', action='store_true',
                      default=False,
                      help='Specify this option, if you also want to delete the'
                           ' pickled state. [default: %default]')
    parser.add_option('--range', dest='range',
                      help='Specify a range of cases to delete.')
    # Parse the options
    (options, args) = parser.parse_args()

    # Sanity check
    check_option('plicked_case')
    if options.range is None:
        case_names = options.pickled_case
    else:
        tmp = options.range.split(':')
        r0 = int(tmp[0])
        r1 = int(tmp[1])
        if len(tmp) == 3:
            r2 = tmp[2]
        else:
            r2 = 1
        case_names = [options.pickled_case + '_%d' % i
                      for i in range(r0, r1, r2)]
    for case_name in case_names:
        # Is it a pickled case or not?
        try:
            with open(case_name, 'rb') as fd:
                case = pickle.load(fd)
            pickle_file = os.path.abspath(case_name)
        except:
            case = clm.Case(name=case_name)
            pickle_file = os.path.abspath(case_name + '.pickle')
        if case.is_created:
            print 'Deleting', case.name
        if case.is_built:
            print 'Deleting', case.ccsm_compile
        if not case.is_built and case.is_clone:
            print 'Deleting', case.ccsm_run
        if not options.just_check:
            case.clean()
        if options.archive and os.path.isdir(case.ccsm_archive):
            print 'Deleting', case.ccsm_archive
            if not options.just_check:
                case.clean_archive()
        if options.pickle and os.path.exists(pickle_file):
            print 'Deleting', pickle_file
            if not options.just_check:
                os.remove(pickle_file)
        elif os.path.exists(pickle_file) and not options.just_check:
            with open(pickle_file, 'wb') as fd:
                pickle.dump(case, fd, pickle.HIGHEST_PROTOCOL)
