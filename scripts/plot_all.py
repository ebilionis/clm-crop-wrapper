"""
Looks at the MCMC data base and plots the results.
"""

import matplotlib.pyplot as plt
import os
import sys
import pysmc as ps
from optparse import OptionParser


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--db-file', dest='db_file',
                      type='string',
                      help='Specify the data base file.')
    (options, args) = parser.parse_args()
    if options.db_file is None:
        sys.stderr.write('You have to specify the database file.\n')
        sys.exit(0)
    var_names = ['leafcn',
                 'fleafcn',
                 'livewdcn',
                 'fstemcn',
                 'frootcn',
                 'organcn']
    prefix = os.path.splitext(options.db_file)[0]
    db = ps.SerialDataBase.load(options.db_file, go_to_last=False)
#    gamma_png = prefix + '_gamma.png'
#    fig = plt.figure()
#    ax = fig.add_subplot(1, 1, 1)
#    ax.plot(db.gamma, '*', markersize=5)
#    ax.set_yscale('log')
#    ax.set_xlabel('SMC step', fontsize=16)
#    ax.set_ylabel('$\gamma$', fontsize=16)
#    plt.savefig(gamma_png)
    while db.next():
        print 'Doing gamma:', db.gamma
        out_png = prefix
        out_png += '_gamma_%d.png' % db.num_gammas
        p = db.particle_approximations
        x = p.input
        w = p.weights
        fig = plt.figure()
        for j in range(x.shape[1]):
            for k in range(j + 1):
                ax.add_subplot(x.shape[1], j, k)
                ax.set_xlabel('%s' % var_names[j], fontsize=16)
                if j == k:
                    ax.set_ylabel('p(%s)' % var_names[j], fontsize=16)
                    ax.hist(x[:, j], weights=w, normed=True, bins=20)
                else:
                    ax.set_ylabel('%s' % var_names[k], fontsize=16)
                    ax.hist2d(x[:, j], x[:, k], weights=w, normed=True,
                              bins=10)
        fig.savefig(out_png)
