#!/bin/env python
"""
This script siply tests how much time it takes to collect
data and creates a few figures.
"""


from common import *
import subprocess
import numpy as np
import time


AVERAGE_RUN_TIME = 3
"""
The average run time of a single instance of CLM in minutes.
"""


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    # Set the options
    add_case_pool_option(parser)
    parser.add_option('--cpu-range', dest='cpu_range', type='string',
                      help='Specify the range of cpus to be tested.'
                           ' It should be of the form:'
                           ' \'init_cpus:times_to_double\''
                           ', where ``init_cpus`` is the initial number of'
                           ' cpus to be used and ``times_to_double`` is how'
                           ' many times you need this to be doubled.')
    add_just_check_option(parser)
    add_pbs_options(parser)
    (options, args) = parser.parse_args()

    # Sanity check
    check_option(options, 'case_pool')
    check_option(options, 'num_samples')
    check_option(options, 'cpu_range')

    # Do the job
    tmp = options.cpu_range.split(':')
    cpus = int(tmp[0])
    times_to_double = int(tmp[1])
    num_samples = options.num_samples
    if options.time is None:
        walltime = "00:%d:00" % (AVERAGE_RUN_TIME * num_samples / cpus)
    else:
        walltime = options.time
    results = []
    for i in xrange(times_to_double):
        print 'Testing the collection of %d samples with %d cpus.' % (num_samples, cpus)
        job_name = 's=%d_test_data_collection' % num_samples
        pbs_script = """
#!/bin/sh
#PBS -A %s
#PBS -N %s
#PBS -l nodes=%d:ppn=%d
#PBS -l walltime=%s
#PBS -j oe


cd $PBS_O_WORKDIR

mpirun -n %d \\
    ../scripts/case sample \\
    -c %s \\
    -s %d \\
    -p ../examples/lognormal_prior_model.py \\
    --use-mpi
""" % (options.allocation, job_name, cpus / options.ppn, options.ppn, walltime,
       cpus, options.pickled_case_or_case_pool, num_samples)
        pbs_file = job_name + '.pbs'
        if not options.just_check:
            with open(pbs_file, 'w') as fd:
                fd.write(pbs_script)
            p = subprocess.Popen(['qsub', pbs_file],
                                 stdout=subprocess.PIPE)
            p.wait()
            if not p.returncode == 0:
                raise RuntimeError('I couldn\'t submit job \'%s\'.' % pbs_file)
            job_id = p.stdout.readline().split('.')[0]
            print 'Waiting for job %s to complete' % job_id
            clm.wait_for_jobs(job_id)
            # Wait for pbs to write the file
            time.sleep(60)
            job_out_file = job_name + '.o' + job_id
            with open(job_out_file, 'r') as fd:
                job_out = fd.read()
            reg = re.compile(r'cput=(\d\d:\d\d:\d\d),mem=(\d+)kb,vmem=(\d+)kb,walltime=(\d\d:\d\d:\d\d)')
            m = reg.search(job_out)
            if m is None:
                raise RuntimeError('I couldn\'t read the job\'s output.')
            cputime = m.group(1)
            memory = m.group(2)
            virtual_memory = m.group(3)
            obs_walltime = m.group(4)
            print 'cputime:', cputime
            print 'memory:', memory
            print 'virtual memory:', virtual_memory
            print 'walltime:', obs_walltime
            results.append([cpus, num_samples, cputime, memory, virtual_memory, obs_walltime])
        num_samples *= 2
        cpus *= 2
    out_name = os.path.splitext(options.pickled_case_or_case_pool) + '_data_collection.txt'
    with open(out_name, 'w') as fd:
        for i in range(len(results)):
            for j in range(len(results[i])):
                fd.write('%s ' % results[i][j])
            fw.write('\n')
