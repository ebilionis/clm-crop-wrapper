#!/bin/env python
"""
This script allows you to collect data from a case by varying some
of the inputs. The inputs are specified through a python module
that contains PyMC random variables with the same names as the inputs
you want to change.
"""


from common import *
import warnings
import numpy as np
import pandas as pd
import traceback


if __name__ == '__main__':
    # How to use the script
    usage = '%prog [options]'
    description = __doc__
    # Initialize the parse
    parser = OptionParser(usage=usage, description=description)
    add_case_pool_option(parser)
    add_num_samples_option(parser)
    add_prior_model_option(parser)
    add_use_mpi_option(parser)

    # Parse the options
    (options, args) = parser.parse_args()
    # Check if MPI is on
    mpi, rank, size = get_mpi(options.use_mpi)

    # Make sure everybody has a different seed
    np.random.seed(314159 * (rank + 1))

    # Sanity check
    check_option(options, 'case_pool', mpi=mpi)
    check_option(options, 'num_samples', mpi=mpi)
    check_option(options, 'prior_model', mpi=mpi)

    # Load the data
    pymc = load_pymc()
    case_pool = initialize_pickled_object(options.case_pool, 'case pool',
                                          mpi=mpi)
    my_case = case_pool['clones'][rank]
    prior_model = initialize_model(options.prior_model, 'prior model', mpi=mpi)
    free_params = prior_model.free_params

    # Get the number of samples to be taken by each process:
    my_num_samples = options.num_samples / size
    # Create a CLM wrapper (the default one)
    f = clm.Wrapper(my_case, free_params=free_params)

    # The store object
    if rank == 0:
        out_file = os.path.abspath(os.path.splitext(options.case_pool)[0])
        out_file += '_data.h5'
        store = pd.HDFStore(out_file)

    # Sample as many times as required
    try:
        for i in xrange(my_num_samples):
            # Draw a random input:
            f.input.fill_random()
            f.run()
            print_once('samples taken: %d\n' % ((i + 1) * size), mpi=mpi)
            print_once('storing data...', mpi=mpi)
            my_in_df = f.input.get_as_dataframe()
            my_out_df = f.output.get_as_dataframe()
            in_list_df = mpi.COMM_WORLD.gather(my_in_df)
            out_list_df = mpi.COMM_WORLD.gather(my_out_df)
            if rank == 0:
                clm.append_data(in_list_df, out_list_df, store)
            print_once(' Done!\n', mpi=mpi)
    except Exception as e:
        traceback.print_exc()
        if options.use_mpi:
            mpi.COMM_WORLD.Abort(1)
